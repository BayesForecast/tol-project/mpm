#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass amsbook
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine basic
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
MPM: Modular Probabilistic Modelling
\end_layout

\begin_layout Author
Víctor de Buen Remiro
\end_layout

\begin_layout Email
vdebuen@bayesforecast.com
\end_layout

\begin_layout Abstract
El propósito del sistema Modular Probabilistic Modelling (MPM) es permitir
 la definición, estimación y explotación de modelos paramétricos bayesianos
 de casi cualquier tipo con el mínimo esfuerzo de programación tanto en
 su diseño como en su explotación, sin necesidad de que el usuario conozca
 los complejos detalles algorítmicos internos, y favoreciendo el reaprovechamien
to del código al máximo.
 
\end_layout

\begin_layout Abstract
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Abstract

\series bold
\size large
\color red
UNDER CONSTRUCTION
\end_layout

\begin_layout Chapter
Diseño modular de modelos
\end_layout

\begin_layout Section
Introducción
\end_layout

\begin_layout Standard
Un modelo probabilístico o modelo estadístico es un conjunto de distribuciones
 de probabilidad alternativas que pretenden describir un fenómeno de interés
 sujeto a incertidumbre mediante relaciones entre una o más variables aleatorias
 expresables matemáticamente.
 
\end_layout

\begin_layout Standard
Estimar el modelo es elegir la opción que mejor representa la realidad según
 cierto criterio.
 Si el conjunto de distribuciones que conforman el modelo es indexable mediante
 un número finito de parámetros diremos que se trata de un modelo paramétrico.
 En adelante todos los modelos serán paramétricos aunque no se especifique.
 Los parámetros se considerarán variables aleatorias definidas de forma
 indirecta por el modelo.
 
\end_layout

\begin_layout Standard
En un modelo bayesiano, a la distribución de los parámetros se le llamará
 distribución a posteriori y su densidad será el producto de la densidad
 de las observaciones condicionadas por los parámetros (verosimilitud) multiplic
ada por la densidad a priori de los parámetros que se establecerá en base
 a la experiencia y conocimientos previos.
 
\end_layout

\begin_layout Standard
Los modelos sencillos se pueden definir analíticamente como una única distribuci
ón de probabilidad vectorial paramétrica, pero en los casos complejos suele
 ser imposible obtener una forma analítica de la distribución en función
 de los parámetros, y en los pocos casos en los que es posible su estimación
 a menudo suele resultar muy poco eficiente.
\end_layout

\begin_layout Standard
El propósito del sistema Modular Probabilistic Modelling (MPM) es permitir
 la definición, estimación y explotación de modelos paramétricos bayesianos
 de casi cualquier tipo con el mínimo esfuerzo de programación tanto en
 su diseño como en su explotación, sin necesidad de que el usuario conozca
 los complejos detalles algorítmicos internos, y favoreciendo el reaprovechamien
to del código almáximo.
\end_layout

\begin_layout Section
Estrategia de simulación
\end_layout

\begin_layout Standard
Los métodos de estimación empleados por MPM serán siempre métodos bayesianos
 de simulación tipo MCMC
\end_layout

\begin_layout Itemize
El método 
\emph on
Metropolis-Hastings
\emph default
 (MH) y otros derivados como 
\emph on
Adaptive Metropolis
\emph default
, 
\emph on
Multiple Try Metropolis
\emph default
, etc.; serán configurables para adaptarse a los distintos tipos de problemas,
 aunque en principio el usuario podrá despreocuparse de esto.
 La modularidad vendrá dada por la forma separable aditiva que suele presentar
 la log-densidad a posteriori de la mayor parte de los modelos estadísticos
 bayesianos.
 Por ejemplo, las densidades a priori son por definición multiplicativas
 con respecto a la verosimilitud, luego su log-densidad se debe sumar a
 la log-verosimilitud.
 Los métodos de generación de candidatos deberán tener en cuenta el dominio
 de los parámetros para mejorar la eficiencia y permitir el uso de parámetros
 discretos, acotados, etc.
\end_layout

\begin_layout Itemize
El método 
\emph on
Metropolis Within Gibbs
\emph default
 (MWG) se usará en casos concretos en los que los métodos MH pueden ser
 ineficientes y existen bloques cuya distribución condicionada al resto
 de parámetros es expresable de forma unívoca y eficiente.
 En estos casos, estos bloques especiales pueden simularse según el método
 de 
\emph on
Gibbs
\emph default
 y el resto se simularán conjuntamente según el método MH seleccionado.
 Un caso particularmente útil es el de los parámetros dinámicos en un modelo
 de series temporales en el que el propio parámetro evoluciona en el tiempo
 para describir más fielmente cambios exógenos inobservables de forma directa.
\end_layout

\begin_layout Section
Niveles informativos de definición de modelos bayesianos
\end_layout

\begin_layout Enumerate

\emph on
Estructura de modelo masivo
\emph default
: definición abstracta de un conjunto de estructuras de modelos en función
 de características definibles externamente.
 Se puede aplicar a dos tareas de modelación de gran importancia
\end_layout

\begin_deeper
\begin_layout Enumerate
Identificación de la estructura más adecuada de entre un conjunto de opciones
 por exploración exhaustiva.
\end_layout

\begin_layout Enumerate
Aplicación a un conjunto de nodos con estructuras semejantes aunque con
 variantes dependientes del nodo.
\end_layout

\end_deeper
\begin_layout Enumerate

\emph on
Estructura de modelo
\emph default
: estructura abstracta de la clase concreta de modelos y todos sus componentes,
 dimensiones, grados, términos explicativos, ...
\end_layout

\begin_layout Enumerate

\emph on
Modelo estimable
\emph default
: definición exhaustiva e inequívoca del modelo que contiene
\end_layout

\begin_deeper
\begin_layout Enumerate
Estructura del modelo: 
\end_layout

\begin_layout Enumerate
Datos de contraste: output, input, prior, fechas, valores iniciales, ...
\end_layout

\end_deeper
\begin_layout Enumerate

\emph on
Modelo simulado
\emph default
: Modelo que ya ha sido simulado por lo que contiene las cadenas de Markov
 y la definición completa del modelo para poder ser explotado.
\end_layout

\begin_layout Chapter
Clases de modelos paramétricos
\end_layout

\begin_layout Standard
Aunque en las versiones iniciales de MPM sólo se dispondrá de una gama muy
 limitada de modelos, la versatilidad del sistema permitirá con el tiempo
 la implementación de prácticamente cualquier modelo estadístico paramétrico
 del que seamos capaces de escribir su función de log-verosimilitud salvo
 una constante, o bien como una sola expresión, o bien como una suma de
 expresiones separables aditivamente, o bien mediante bloques de Gibbs.
\end_layout

\begin_layout Standard
Los modelos estadísticos paramétricos se pueden clasificar según diversos
 criterios que pueden ser complementarios o alternativos según las combinaciones
 particulares a que dan lugar.
 No existe en la literatura, o al menos no hemos sido capaces de encontrarla,
 una taxonomía completa de los modelos estadísticos paramétricos, por lo
 que se intentará a continuación definir una clasificación que al menos
 recoja las clases de modelos más conocidas y todas las que han sido utilizadas
 en Bayes o puedan serlo previsiblemente en un futuro.
\end_layout

\begin_layout Section
Modelos de regresión
\end_layout

\begin_layout Standard
En los modelos de regresión se trata de explicar y/o prever el comportamiento
 de una o más variables dependientes, en función de otras variables independient
es conocidas (factores exógenos), o bien de su propia historia e inercia
 (factores endógenos), o bien de ambos tipos de factores, y todo ello en
 presencia de un ruido (factores no explicados) que incorpora todos los
 factores inherentemente aleatorios así como aquellos otros que no son observabl
es o que, consciente o inconscientemente, no se hayan tenido en cuenta de
 forma explícita y separada.
\end_layout

\begin_layout Subsection
Tipos de regresiones en función del output
\end_layout

\begin_layout Subsubsection
Clasificación según la distribución de probabilidad
\end_layout

\begin_layout Enumerate
Regresión lineal generalizada (distribuciones de la familia exponencial)
\end_layout

\begin_deeper
\begin_layout Enumerate
Distribuciones continuas
\end_layout

\begin_deeper
\begin_layout Enumerate
Trans-normal
\end_layout

\begin_deeper
\begin_layout Enumerate
Box-Cox Normal : Normal, Log-Normal, Power-Normal
\end_layout

\begin_layout Enumerate
Proporciones normalizadas : logit-normal, probit-normal, ...
\end_layout

\end_deeper
\begin_layout Enumerate
Tiempo de supervivencia: Exponential, Weibull, Exponential-logarithmic
\end_layout

\end_deeper
\begin_layout Enumerate
Distribuciones cualitativas
\end_layout

\begin_deeper
\begin_layout Enumerate
Binomiales: Probit, Logit, ...
\end_layout

\begin_layout Enumerate
Multinomiales
\end_layout

\end_deeper
\begin_layout Enumerate
Distribuciones discretas
\end_layout

\begin_deeper
\begin_layout Enumerate
Distribuciones de conteo: Poisson, Geométrica, Binomial negativa, Generalizada,
 ...
\end_layout

\end_deeper
\end_deeper
\begin_layout Enumerate
Distribuciones no exponenciales
\end_layout

\begin_deeper
\begin_layout Enumerate
Extreme Value Regression
\end_layout

\begin_layout Enumerate
Cauchy regression
\end_layout

\begin_layout Enumerate
Mixed regression
\end_layout

\begin_layout Enumerate
Zero Inflated Regression
\end_layout

\begin_layout Enumerate
Tobit regression (normal truncada)
\end_layout

\end_deeper
\begin_layout Subsubsection
Clasificación según la dispersión
\end_layout

\begin_layout Enumerate
Modelos homocedásticos: La dispersión de las perturbaciones es homogénea
 en toda la muestra.
\end_layout

\begin_layout Enumerate
Modelos heterocedásticos: La dispersión de las perturbaciones no es la misma
 en todas las observaciones.
\end_layout

\begin_deeper
\begin_layout Enumerate
Modelos ponderados: La dispersión asociada a cada observación es inversamente
 proporcional a su ponderación dada por cierto criterio de credibilidad
 o importancia de dicha observación.
\end_layout

\begin_layout Enumerate
Modelos de dispersión dinámica: Modelos de series temporales en los que
 la propia dispersión es un proceso estocástico inobservable (ARCH, GARCH,
 APARCH,...)
\end_layout

\end_deeper
\begin_layout Subsubsection
Clasificación según la indexación del output
\end_layout

\begin_layout Enumerate
Output no secuencial
\end_layout

\begin_deeper
\begin_layout Enumerate
Indexación de dimensiones arbitrarias
\end_layout

\begin_layout Enumerate
Indexación geométrica (recta, plano, ...)
\end_layout

\end_deeper
\begin_layout Enumerate
Serie temporal
\end_layout

\begin_deeper
\begin_layout Enumerate
Serie temporal discretizada : ARIMA, ARFIMA, ...
\end_layout

\begin_layout Enumerate
Serie temporal continua : Poisson process, CARMA, ...
\end_layout

\end_deeper
\begin_layout Subsubsection
Clasificación según la dimensión del output
\end_layout

\begin_layout Enumerate
Univariante
\end_layout

\begin_layout Enumerate
Multivariante
\end_layout

\begin_deeper
\begin_layout Enumerate
Vectorial
\end_layout

\begin_layout Enumerate
Vectorial temporal: VAR, VARMA, VECAR, VECARMA
\end_layout

\begin_layout Enumerate
De panel
\end_layout

\begin_layout Enumerate
En cascada: agregaciones geográficas, ...
\end_layout

\end_deeper
\begin_layout Subsubsection
Conocimiento parcial del output
\end_layout

\begin_layout Standard
Aunque la estructura conceptual de un modelo no cambia por el hecho de que
 haya un conocimiento parcial del output, sí que afecta al método de estimación
 que deberá completar la información faltante de manera congruente con los
 datos y la información a priori disponible.
\end_layout

\begin_layout Enumerate
Datos omitidos
\end_layout

\begin_layout Enumerate
Datos censurados
\end_layout

\begin_layout Subsection
Tipos de términos explicativos
\end_layout

\begin_layout Subsubsection
Clasificación según la forma funcional del filtro
\end_layout

\begin_layout Enumerate
Lineal
\end_layout

\begin_layout Enumerate
Función de transferencia temporal 
\begin_inset Formula $\nicefrac{\omega\left(B\right)}{\delta\left(B\right)}$
\end_inset


\end_layout

\begin_layout Enumerate
Spline : Orden 1 (Piecewise), Orden 3 (Spline Cúbico de Trazador)
\end_layout

\begin_layout Enumerate
Funcional paramétrica
\end_layout

\begin_deeper
\begin_layout Enumerate
Polinomial
\end_layout

\begin_deeper
\begin_layout Enumerate
Natural
\end_layout

\begin_layout Enumerate
Ortogonal: Chevyshev, Hermite, Laguerre, ...
\end_layout

\end_deeper
\begin_layout Enumerate
Fourier
\end_layout

\begin_layout Enumerate
Wavelets
\end_layout

\end_deeper
\begin_layout Subsubsection
Clasificación según la métrica de filtrado
\end_layout

\begin_layout Enumerate
Filtrado aditivo a nivel del residuo: regresión lineal, X-ARIMA, X-VAR
\end_layout

\begin_layout Enumerate
Filtrado aditivo a nivel de la función de enlace: regresión lineal generalizada,
 ARIMA-TF, VAR-TF, ...
\end_layout

\begin_layout Enumerate
Filtrado aditivo a nivel del output en términos originales: filtros previos
 a la transformación del output
\end_layout

\begin_layout Subsubsection
Clasificación según la naturaleza estocástica de los términos explicativos
\end_layout

\begin_layout Enumerate
Observados:
\end_layout

\begin_deeper
\begin_layout Enumerate
Inputs deterministas
\end_layout

\begin_layout Enumerate
Inputs estocásticos
\end_layout

\end_deeper
\begin_layout Enumerate
Latentes:
\end_layout

\begin_deeper
\begin_layout Enumerate
Hiperparámetros
\end_layout

\begin_layout Enumerate
Parámetros dinámicos: 
\end_layout

\end_deeper
\begin_layout Section
Clasificación según la información a priori disponible
\end_layout

\begin_layout Enumerate
Libre: sin ningún tipo de información a priori
\end_layout

\begin_layout Enumerate
Prior no informativo (inecuaciones):
\end_layout

\begin_deeper
\begin_layout Enumerate
Restricciones de dominio
\end_layout

\begin_layout Enumerate
Restricciones de orden
\end_layout

\begin_layout Enumerate
Restricciones sobre combinaciones lineales 
\end_layout

\begin_layout Enumerate
Restricciones arbitrarias
\end_layout

\end_deeper
\begin_layout Enumerate
Prior informativos
\end_layout

\begin_deeper
\begin_layout Enumerate
Sobre los parámetros de los términos explicativos
\end_layout

\begin_deeper
\begin_layout Enumerate
Normal escalar
\end_layout

\begin_layout Enumerate
Normal vectorial
\end_layout

\end_deeper
\begin_layout Enumerate
Sobre los parámetros de dispersión
\end_layout

\begin_deeper
\begin_layout Enumerate
Inverse Chi-square
\end_layout

\begin_layout Enumerate
Inverse Wishart
\end_layout

\end_deeper
\end_deeper
\begin_layout Enumerate
Reducción dimensional
\end_layout

\begin_deeper
\begin_layout Enumerate
Parámetros fijos
\end_layout

\begin_layout Enumerate
Parámetros compartidos
\end_layout

\begin_layout Enumerate
Parámetros marginales (pre-simulados)
\end_layout

\begin_layout Enumerate
Ecuaciones de igualdad lineales
\end_layout

\begin_layout Enumerate
Ecuaciones de igualdad arbitrarias
\end_layout

\end_deeper
\begin_layout Section
Modelos de clasificación
\end_layout

\begin_layout Subsection
Análisis de varianza
\end_layout

\begin_layout Subsection
Análisis discriminante
\end_layout

\begin_layout Subsection
Clustering
\end_layout

\begin_layout Subsection
Árboles de decisión
\end_layout

\begin_layout Chapter
API de construcción de modelos en MPM
\end_layout

\begin_layout Standard
Las clases de MPM se dividen en las de uso interno (Engine) y las de uso
 externo (API), y éstas últimas se especializan en los tres subgrupos que
 se estudian a continuación.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset Graphics
	filename API.png

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Manejo de datos
\end_layout

\begin_layout Standard
Estas clases TOL se ocupan del almacenamiento interno de los datos que pueden
 provenir de matrices, series temporales, y en un futuro de bases de datos
 u otro tipo de consultas remotas.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset Graphics
	filename API.DataHandling.png

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Section
Componentes de modelos
\end_layout

\begin_layout Standard
Los componentes de la estructura de un modelo que pueden aparecer en una
 o varias clases de modelos, como términos explicativos, priors, estructuras
 GARCH, cointegración, etc.; se definen de forma autónoma para facilitar
 el desarrollo modular.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset Graphics
	filename API.Component.png

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Clases de modelos
\end_layout

\begin_layout Standard
Las clases de modelos implementadas hasta ahora son todas regresiones desarrolla
das en una estructura jerárquica modular de clases 
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset Graphics
	filename API.ModelClass.png

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Chapter
Diagnosis
\end_layout

\begin_layout Section
Diseño
\end_layout

\begin_layout Section
Convergencia
\end_layout

\begin_layout Section
Tests de hipótesis
\end_layout

\begin_layout Chapter
Inferencia
\end_layout

\begin_layout Section
Previsión
\end_layout

\begin_layout Section
Decisión bayesiana
\end_layout

\end_body
\end_document
