/////////////////////////////////////////////////////////////////////////////
// FILE: DataSource.tol
// PURPOSE: Vectorial data handling
/////////////////////////////////////////////////////////////////////////////

Class @DataSource.Empty;
Class @DataSource.V;
Class @DataSource.M;
Class @DataSource.VS;
Class @DataSource.MS;
Class @DataSource.SS;


/////////////////////////////////////////////////////////////////////////////
Class @DataSource : @APIDataHandling
/////////////////////////////////////////////////////////////////////////////
{
  Set _obj;
  Set _nodeNames = Copy(Empty);
  Real getNodeNumber(Real void);
  Set getNodeNames(Real void) { _nodeNames };
  VMatrix getAll(Real void);
  VMatrix getCol(Real node);
  Serie getSerie(Real node) 
  { 
    Error("Not implemented method "+ClassOf(_this)+"::getSerie");
    CalInd(W,W)
  };
  Real setAll(VMatrix x);
  Real setCol(Real node, VMatrix x);
  Set getObj(Real void) { _obj };
  Real hasTimeInfo(Real void) { False };
  Set getTimeInfo(Real void) { Copy(Empty) };
  Real getLength(Real void);
   
  ///////////////////////////////////////////////////////////////////////////
  Real buildNames(Set names)
  ///////////////////////////////////////////////////////////////////////////
  {
    Set _nodeNames := EvalSet(names,Text(Text name)
    {
      Eval(ToName(name)+"=name")
    });
    Real SetIndexByName(_nodeNames);
    True
  };
  
  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource Create.Empty(Real void)
  ///////////////////////////////////////////////////////////////////////////
  {
    @DataSource.Empty new = [[ 
      Set _obj = Copy(Empty)
    ]];
    new
  };
  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource Create.V(VMatrix x, Set nodeNames)
  ///////////////////////////////////////////////////////////////////////////
  {
    @DataSource.V new = [[ 
      Set _obj = @VMatrix(x) 
    ]];
    Set nod = If(Card(nodeNames),nodeNames,
    {
      For(1,VColumns(x),Text(Real j)
      {
        Name(x)+If(VColumns(x)==1,"","."<<j)
      })
    });
    Real new::buildNames(nod);
    new
  };
  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource Create.M(Matrix x, Set nodeNames)
  ///////////////////////////////////////////////////////////////////////////
  {
    @DataSource.M new = [[ 
      Set _obj = @Matrix(x) 
    ]];
    Set nod = If(Card(nodeNames),nodeNames,
    {
      For(1,Columns(x),Text(Real j)
      {
        Name(x)+If(Columns(x)==1,"","."<<j)
      })
    });
    Real new::buildNames(nod);
    new
  };

  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource Create.VS(Set x, Set nodeNames)
  ///////////////////////////////////////////////////////////////////////////
  {
    @DataSource.VS new = [[ 
      Set _obj = x 
    ]];
    Set nod = If(Card(nodeNames),nodeNames,
    {
      For(1,VColumns(x),Text(Real j)
      {
        Name(x)+If(VColumns(x)==1,"","."<<j)
      })
    });
    Real new::buildNames(nod);
    new
  };
  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource Create.MS(Set x, Set nodeNames)
  ///////////////////////////////////////////////////////////////////////////
  {
    @DataSource.MS new = [[ 
      Set _obj = x 
    ]];
    Set nod = If(Card(nodeNames),nodeNames,
    {
      For(1,Columns(x),Text(Real j)
      {
        Name(x)+If(Columns(x)==1,"","."<<j)
      })
    });
    Real new::buildNames(nod);
    new
  };
  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource Create.SS(Set x, Set nodeNames)
  ///////////////////////////////////////////////////////////////////////////
  {
    @DataSource.SS new = [[ 
      Text _name = Name(x);
      Set _obj = x 
    ]];
    Real new::setAutoDates(?);
    Set nod = If(Card(nodeNames),nodeNames,EvalSet(x,Name));
    Real new::buildNames(nod);
    new
  };

  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource CreateFromSetOfSerie(
    Set x, Date firstDate, Date lastDate)
  ///////////////////////////////////////////////////////////////////////////
  {
    @DataSource.SS new = [[ 
      Text _name = Name(x);
      Date _firstDate = firstDate;
      Date _lastDate = lastDate;
      Set _obj = x 
    ]];
    Set nod = EvalSet(x,Name);
    Real new::buildNames(nod);
    new
  };
  
  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource CreateWithNames(Anything x, Set nodeNames)
  ///////////////////////////////////////////////////////////////////////////
  {
    Text g = Grammar(x);
    Case(
      g=="VMatrix", @DataSource::Create.V(x,nodeNames),
      g=="Matrix",  @DataSource::Create.M(x,nodeNames),
      g=="Serie",   @DataSource::Create.SS([[x]],nodeNames),
      g=="Set", 
      {
        If(Card(x)==0, @DataSource::Create.Empty(?),
        {
          Text g1 = Grammar(x[1]);
          Case(
            g1=="VMatrix", @DataSource::Create.VS(x,nodeNames),
            g1=="Matrix",  @DataSource::Create.MS(x,nodeNames),
            g1=="Serie",   @DataSource::Create.SS(x,nodeNames),
          True, Error("[MPM::@DataSource::CreateWithNames] "
                      "Cannot handle with type SetOf"<<g1))
        })
      },
      True, Error("[MPM::@DataSource::CreateWithNames] "
                  "Cannot handle with type "<<g)
    )
  };
  ///////////////////////////////////////////////////////////////////////////
  Static @DataSource Create(Anything x)
  ///////////////////////////////////////////////////////////////////////////
  {
    @DataSource::CreateWithNames(x,Empty)
  } 
};

/////////////////////////////////////////////////////////////////////////////
Class @DataSource.Empty : @DataSource
/////////////////////////////////////////////////////////////////////////////
{
  Set _obj = Copy(Empty);
  Real getNodeNumber(Real void) { 0 };
  Real getLength(Real void) { 0 };
  VMatrix getCol(Real node) { Constant(0,1,?) };
  VMatrix getAll(Real void) { Constant(0,0,?) };
  Real setAll(VMatrix x) 
  { 
    False
  };
  Real setCol(Real node, VMatrix x)
  {
    False
  }
};

/////////////////////////////////////////////////////////////////////////////
Class @DataSource.V : @DataSource
/////////////////////////////////////////////////////////////////////////////
{
  Real getNodeNumber(Real void) { VColumns($_obj) };
  Real getLength(Real void) { VRows($_obj) };
  VMatrix getCol(Real node) { SubCol($_obj,[[node]]) };
  VMatrix getAll(Real void) { $_obj };
  Real setAll(VMatrix x) 
  { 
    VMatrix $_obj := x;
    True
  };
  Real setCol(Real node, VMatrix x)
  {
    PutVMatBlock($_obj,1,node,x)
  }
};

/////////////////////////////////////////////////////////////////////////////
Class @DataSource.M : @DataSource
/////////////////////////////////////////////////////////////////////////////
{
  Real getNodeNumber(Real void) { Columns($_obj) };
  Real getLength(Real void) { Rows($_obj) };
  VMatrix getCol(Real node) { Mat2VMat(SubCol($_obj,[[node]])) };
  VMatrix getAll(Real void) { Mat2VMat($_obj) };
  Real setAll(VMatrix x) 
  { 
    Matrix $_obj := VMat2Mat(x);
    True
  };
  Real setCol(Real node, VMatrix x)
  {
    PutMatBlock($_obj,1,node,x)
  }
};

/////////////////////////////////////////////////////////////////////////////
Class @DataSource.VS : @DataSource
/////////////////////////////////////////////////////////////////////////////
{
  Real getNodeNumber(Real void) { Card(_obj) };
  Real getLength(Real void) { VRows(_obj[1]) };
  VMatrix getCol(Real node) { _obj[node] };
  VMatrix getAll(Real void) { Group("ConcatColumns",_obj) };
  Real setAll(VMatrix x)
  {
    Set For(1,Card(_obj),Real(Real j)
    {
      VMatrix _obj[node] := SubCol(x,[[j]]);
      True 
    });
    True
  };
  Real setCol(Real node, VMatrix x)
  {
    Set _obj[node] := @VMatrix(x);
    True
  }
};

/////////////////////////////////////////////////////////////////////////////
Class @DataSource.MS : @DataSource
/////////////////////////////////////////////////////////////////////////////
{
  Real getNodeNumber(Real void) { Card(_obj) };
  Real getLength(Real void) { Rows(_obj[1]) };
  VMatrix getCol(Real node) { _obj[node] };
  VMatrix getAll(Real void) { Mat2VMat(Group("ConcatColumns",_obj)) };
  Real setAll(VMatrix x)
  {
    Set For(1,Card(_obj),Real(Real j)
    {
      Matrix _obj[node] := VMat2Mat(SubCol(x,[[j]]));
      True 
    });
    True
  };
  Real setCol(Real node, VMatrix x)
  {
    Set _obj[node] := @VMatrix(x);
    True
  }
};

/////////////////////////////////////////////////////////////////////////////
Class @DataSource.SS : @DataSource
/////////////////////////////////////////////////////////////////////////////
{
  Date _firstDate = TheBegin;
  Date _lastDate = TheEnd;
  Real setAutoDates(Real void)
  {
    Date _firstDate := SetMinDate(EvalSet(_obj,First));
    Date _lastDate  := SetMaxDate(EvalSet(_obj,Last ));
    True
  };

  Real hasTimeInfo(Real void) { True };
  Set getTimeInfo(Real void) 
  { 
    @BSR.NoiseTimeInfo(Dating(_obj[1]), _firstDate, _lastDate)
  };
  Real getNodeNumber(Real void) { Card(_obj) };
  Real getLength(Real void) { CountS(_obj[1], _firstDate, _lastDate) };
  VMatrix getCol(Real node) 
  { 
    Mat2VMat(SerSetMat([[ _obj[node] ]],_firstDate,_lastDate),True) 
  };
  VMatrix getAll(Real void) 
  { 
    Mat2VMat(SerSetMat(_obj,_firstDate,_lastDate),True) 
  };
  Real setCol(Real node, VMatrix x)
  {
    Set _obj[node] := MatSerSet(VMat2Mat(x,True),Dating(_obj[1]),_firstDate)[1];
    True
  };
  Serie getSubSerie(Real node, Date fst, Date lst) 
  {
    Serie s = SubSer(_obj[node],fst,lst);
    Eval(_nodeNames[node]+"=s")
  };  
  Serie getSerie(Real node) 
  {
    getSubSerie(node,_firstDate,_lastDate)
  };  
  Set getSubSeries(Date fst, Date lst) 
  {
    For(1,Card(_nodeNames),Serie(Real node)
    {
      getSubSerie(node,fst,lst) 
    })
  };  
  Set getSeries(Real void) 
  {
    getSubSeries(_firstDate,_lastDate)
  };  
  Real setSerie(Serie s) 
  {
    Real node = FindIndexByName(_nodeNames, Name(s));
    If(!node,False,
    {
      Serie _obj[node] := s;
      True
    })
  };  
  Real setAll(VMatrix x)
  {
    Set _obj := MatSerSet(VMat2Mat(x,True),Dating(_obj[1]),_firstDate);
    True
  }
      
};


