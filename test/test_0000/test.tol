#Require MPM;


Real PutRandomSeed(123);
Real rndSeed = GetRandomSeed(0);
WriteLn("Current Random Seed = "<<rndSeed);

#Embed "init.tol";

NameBlock trueModel = {
  @TrueModel.LinReg.Normal.Def trueModelDef = [[
    Real varNum = IntRand(10,10);
    Real rank = varNum-1;
    Real dataNum = varNum*IntRand(3,5);
    Real missingNum = IntRand(0,Max(3,dataNum*2/365))
  ]];
  trueModelDef::Create(?)
};

/* */
Real sigma = trueModel::sigma2^0.5;
VMatrix b.true = trueModel::param;
Matrix b.true_ := VMat2Mat(b.true);
Real n = trueModel::n;
Serie resid.true = trueModel::e;
Real stdErr.true = StDsS(resid.true);
Real logDens.true = ?;
Real logDens.est = ?;
Real logDens.opt = ?;
Real logDens.nom = ?;
Real logDens.bys = ?;

//Creating an empty MPM definition
MPM::@NormalReg mpm = [[ Text name = "test_0000" ]];

//Two ways of setting options
Real mpm::setOption("autoPrior.normal.zeroMean.bounded.numSigma", Real 1);
Real mpm::config(?)::autoPrior.maxAbsoluteParamValue := 10;
Real mpm::config(?)::partialEvaluation := False;
Text mpm::config(?)::nlopt.algorithm  :=  "LN_SBPLX";
Real mpm::config(?)::nlopt.verboseEach := 100;
Real mpm::config(?)::nlopt.absoluteTolerance_target := ?;
Real mpm::config(?)::nlopt.relativeTolerance_target := 1E-7;
Real mpm::config(?)::mcmc.maxSize := 5000;
Real mpm::config(?)::mcmc.burnin := 1000;
Real mpm::config(?)::mcmc.thinning := 1;
Real mpm::config(?)::mcmc.cache := 100;
Text mpm::config(?)::mcmc.sampler.class := MPM::Options::SamplerClass::MH.RWB;

//Setting output data source
Real mpm::setOutputDataSource( MPM::@DataSource::Create(trueModel::Y) );
//Setting output data source (direct mode)
//MPM::@DataSource mpm::Y := MPM::@DataSource::Create(trueModel::Y);

Real mpm::setTransformation(
  MPM::@Transform.Identify::Create(?)
//MPM::@Transform.BoxCox::CreateIdentity(?)
//MPM::@Transform.Linear::Create(0,1)
);

//Creating linear explicative filter
MPM::@LinearBlock beta = [[
  Text name = "beta";
  Text latexName = "\beta";
  Set paramNames = trueModel::LinBlk.names;
  MPM::@DataSource X = MPM::@DataSource::CreateWithNames(trueModel::X,paramNames)
]];

//Setting prior information
Real { beta::appendPrior(MPM::@ScalarNormalPrior::Create(
  For(1,n,Set(Real k)
  {
    BysPrior::@PsbTrnNrmUnfSclDst(trueModel::LinBlk.names[k],?,?,-2,2)
  })
))};

//Adding filter to MPM definition
Real mpm::appendFilter(beta);

//Checks and setup internal members of MPM defintion
Real mpm::setup(?);

//Creates the MPM estimation engine
MPM::@Estimator mpm.engine = mpm::CreateEstimator(?);

Real mpm.engine::setup(?);

VMatrix b0 = mpm.engine::get.x(?);
Real logDens.0 = mpm.engine::pi_logDens(b0);

/*
Text latex = mpm.engine::getLatexEquation(?);

//Real mpm.engine::setInitialValues.auto(?);
Real  mpm.engine::buildGraphGexf("mpm.gexf");

/* */
WriteLn("=================================================");
Real logDens.true := mpm.engine::pi_logDens_M(b.true_);
Real mpm.engine::set.x(b.true);
Set status.true = mpm.engine::getStatus(?);

WriteLn("=================================================");
Serie resid.true;
Serie resid.true_  = MatSerSet(VMat2Mat(mpm.engine::getModule("residuals")::_.noise,True),
  Dating(resid.true),First(resid.true))[1];
Serie w.true_  = resid.true_;
Serie z.true_  = resid.true_;

Serie trueModel::F;
Serie F.true_  = MatSerSet(VMat2Mat($(mpm.engine::getModule("beta")::_.X)*
  mpm.engine::getModule("beta")::get.x(?),True),
  Dating(resid.true),First(trueModel::F))[1];

Real stdErr.true;
Real stdErr.true_ = StDsS(resid.true_);

WriteLn("sigma2="<<Real (sigma^2));
WriteLn("sigma="<<sigma);
WriteLn("stdErr.true="<<stdErr.true);
WriteLn("logDens.true="<<logDens.true);


Set estimate = Include("estimate.tol");
Real mpm.engine::set.x(b0);
Set status.ini = mpm.engine::getStatus(?);

Set eval = mpm::doEvaluation(mpm.engine);

Set nlopt    = Include("nlopt.tol");

Set mcmc     = Include("mcmc.tol");

Set stats = {[[
Set {[[
  Text method = "true", 
  Real stdErr=stdErr.true, 
  Real logDens=logDens.true, 
  Real time=0
]]},
Set {[[
  Text method = "estimate", 
  Real stdErr=stdErr.est, 
  Real logDens=logDens.est, 
  Real time=time.est
]]},
Set {[[
  Text method = "nlopt", 
  Real stdErr=stdErr.opt, 
  Real logDens=logDens.opt, 
  Real time=time.opt
]]},
Set {[[
  Text method = "nomad", 
  Real stdErr=stdErr.nom, 
  Real logDens=logDens.nom, 
  Real time=time.nom
]]},
Set {[[
  Text method = "mcmc", 
  Real stdErr=stdErr.bys, 
  Real logDens=logDens.bys, 
  Real time=time.bys
]]}

]]};

Matrix param.cmp.all = b.true_ | b.est_ | b.opt_ | b.nom_ | b.bys_;

/* */

