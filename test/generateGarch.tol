#Require VariableChange;
#Require MPM;

////////////////////////////////////////////////////////////////////////////////
NameBlock _generateGarch(MPM::@GarchDef garchDef, Real m_, Real dataNum)
////////////////////////////////////////////////////////////////////////////////
{

  Real m = 2*m_;
  Real sigma0 = Rand(0.001, 0.05);
  
  MPM::@ModuleGarch garch = MPM::@ModuleGarch::Create("GARCH",garchDef);
  Real garch::setup(?);  

  Real _reset = {
    Real n = garch::_.p + garch::_.q;
    Matrix z = If(n==1, Rand(1,1,0,1),
    {
      Matrix x = Rand(1,1,0,1) | If(n==2, 
        Rand(1,1,  0,2*Pi),
        Rand(1,n-2,0,2*Pi) | Rand(1,1,0,Pi) );
      Matrix y = VariableChange::CppTools::ChgVar.InsideHypersphere(x);
      y^2
    }) * 0.95;
    Real k = 1;
    Polyn garch::_.beta := 1 - SetSum(For(1,garch::_.p,Polyn(Real deg)
    {
      Real c = MatDat(z,1,k);
      Real k:=k+1;
      c*B^deg
    })) ;
    Polyn garch::_.alpha := SetSum(For(1,garch::_.q,Polyn(Real deg)
    {
      Real c = MatDat(z,1,k);
      Real k:=k+1;
      c*B^deg
    }));
    Real garch::set.garch.auto(?)
  };

  Real mxpq = Max(garch::_.p,garch::_.q);
  
  Real delta = garch::_.aparchDelta;
  Matrix u = Gaussian(m,1,0,1);
  
  Set ud = MatSet(Tra(Abs(u)^delta))[1];
  Set sd = NCopy(m,sigma0^delta);
  Set a = For(1,garch::_.q,Real(Real deg) { Coef(garch::_.alpha,deg) });
  Set b = For(1,garch::_.p,Real(Real deg) { -Coef(garch::_.beta,deg) });
  Real a0 = If(garch::hasConstant, garch::_.alpha0, 0);
  Set For(mxpq+1,m,Real(Real k)
  {
    Real sd[k] := a0 + 
    If(Not(garch::_.q),0,SetSum(For(1,garch::_.q,Real(Real j)
    {
      a[j]*sd[k-j]*ud[k-j]
    }))) + 
    If(Not(garch::_.p),0,SetSum(For(1,garch::_.p,Real(Real j)
    {
      b[j]*sd[k-j]
    })))
  });
  Real t1 = m - m_+1;
  Real t2 = m - dataNum+1;
  Matrix stdDev = Sub(SetCol(sd)^(1/delta),t1,1,m_,1);
  Matrix stdDev2 = Sub(SetCol(sd)^(1/delta),t2,1,dataNum,1);
  Matrix homocedastic = Sub(u,t1,1,m_,1);
  Matrix residual = homocedastic $* stdDev;
  Real stdDevMin = MatMin(stdDev2);
  Real stdDevAvr = MatAvr(stdDev2);
  Real stdDevMax = MatMax(stdDev2);
  [[garch, homocedastic, stdDev, stdDevAvr, stdDevMin, stdDevMax, residual]]
};

////////////////////////////////////////////////////////////////////////////////
NameBlock generateGarch(
  MPM::@GarchDef garchDef, 
  Real m, 
  Real dataNum,
  Real minHeterochedasticRatio
  )
////////////////////////////////////////////////////////////////////////////////
{
  Real garch.ok = False;
  Real garch.try = 0;
  NameBlock garch = [[Real unused=?]];
  While(!garch.ok,
  {
    Real garch.try := garch.try +1;
    WriteLn("Heterokedastic GARCH generation try "<<garch.try);
    NameBlock garch := _generateGarch(garchDef,m,dataNum);
    Real garch.ok := GE(garch::stdDevMax / garch::stdDevMin , 
                        minHeterochedasticRatio)
  });
  garch
};


/* * /

NameBlock generateGarch(
  MPM::@GarchDef garchDef = [[
    Real p = IntRand(0,2);
    Real q = IntRand(1,2);
    Real hasConstant = True;
    Real hasInitRes = False;
    Real hasInitSig = False;
    Real hasAparchGamma = False;
    Real hasAparchDelta = False;
    Real aparchDelta = Rand(2,2)  
  ]],
  1000,
  300,
  2);

/* */

