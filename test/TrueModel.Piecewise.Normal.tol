

////////////////////////////////////////////////////////////////////////////////
Class @TrueModel.Piecewise.Normal.Def
////////////////////////////////////////////////////////////////////////////////
{

  Real minPieces = 10;
  Real maxPieces = 10;
  Real minFun = 3;
  Real maxFun = 3;
  Real minResponseSurface = 3;
  Real maxResponseSurface = 5;
  Real minSigmaCoef = .01;
  Real maxSigmaCoef = .40;

  NameBlock Create(Real void)
  {
    Set aux = 
    [[
      Real numFun = IntRand(minFun,maxFun);
      Set numPieces = For(1,numFun,Real(Real f)
      {
        IntRand(minPieces,maxPieces)
      });
      Real totPieces = SetSum(numPieces);
      //WriteLn("@Piecewise.Normal totPieces="<<totPieces);
      Real m = 
        Round(totPieces*Rand(minResponseSurface,maxResponseSurface));
      //WriteLn("@Piecewise.Normal m="<<m);
      Set fun = For(1,numFun,Set(Real f)
      {[[
        Text name = "Piecewise."<<f;
        Real n = numPieces[f];
        Set breakPoint = {
          Matrix bp = 
            DifEq(1/(1-B),Rand(n+1,1,0.01,3),Rand(1,1,-3,3)) + Real (Rand(-3,3));
          Set MatSet(Tra(bp))[1]
        };
        Real derivative.1.sign = IntRand(-1,1);
        Real derivative.2.sign = (-1)^IntRand(0,1);
        Real lowerBound = If(derivative.1.sign>0,0,Rand(-10,0));
        Real upperBound = If(derivative.1.sign<0,0,Rand(0,+10));
        Real length = (upperBound-lowerBound)/n;
        VMatrix beta = Mat2VMat(SetCol(Case(
          derivative.2.sign==0, 
          {
            For(1,n,Real(Real k) { Rand(lowerBound, upperBound) })
          },
          derivative.2.sign>0, 
          {
            Real last = Copy(lowerBound);
            For(1,n,Real(Real k) { Copy(last := Rand(last, lowerBound+k*length)) })
          },
          derivative.2.sign<0, 
          {
            Real last = Copy(upperBound);
            For(1,n,Real(Real k) { Copy(last := Rand(upperBound-k*length,last)) })
          }
        )));
        VMatrix x = Rand(m,1,breakPoint[1],breakPoint[n+1]);
        VMatrix X = Group("ConcatColumns", For(1,n,VMatrix(Real k)
        {
          Real a = breakPoint[k];
          Real b = breakPoint[k+1];
          VMatrix aux = Min(Max(x,a),b)-a
        }));
        VMatrix F = X * beta;
        VMatrix Fx = x | F
      ]]});
      VMatrix X = Group("ConcatColumns",EvalSet(fun,VMatrix(Set f) { f::X }));
      VMatrix beta = Group("ConcatRows",EvalSet(fun,VMatrix(Set f) { f::beta }));
      VMatrix F = X * beta;    
      Real n = VRows(beta);
      Real sigma = Rand(minSigmaCoef,maxSigmaCoef) * VMatStDs(F);
      Real sigma2 = sigma^2;
      VMatrix E = Gaussian(m,1,0,sigma);
      VMatrix Y = F + E;
      VMatrix YF = Y | F;
      VMatrix param = Constant(1,1,sigma2) << beta
    ]];
    SetToNameBlock(aux)
  }
};

