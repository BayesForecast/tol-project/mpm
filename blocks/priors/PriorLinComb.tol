//////////////////////////////////////////////////////////////////////////////
// FILE    : PriorLinComb.tol
// PURPOSE : Implementation of Class MPM::@PriorLinComb
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @PriorLinComb : @Engine
//////////////////////////////////////////////////////////////////////////////
{
  //Combination identifiers
  Set _.combNames = Copy(Empty);
  Text _.latexIneq = "";
  //Combination matrix
  @VMatrix _.comb = [[ VMatrix Constant(0,0,?) ]];
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexIneq(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!TextLength(_.latexIneq),"",
"
\[
\begin{array}{ccc}
"+_.latexIneq+"
\end{array}
\]
")
  };
 
  ////////////////////////////////////////////////////////////////////////////
  Static VMatrix BuildCombRow(@Module module, Set parameters, Set coefficients) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real ok = True;
  //WriteLn("TRACE [MPM::@PriorLinComb::BuildCombRow] 1 module: "<<ClassOf(module)+" "+module::_.name);   
  //WriteLn("TRACE [MPM::@PriorLinComb::BuildCombRow] 2 parameters:\n"<<parameters);   
    Matrix ijx = SetMat(SetConcat(For(1,Card(parameters),Set(Real k)
    {
      Real i = 1;
      Real j = module::findParam(parameters[k]);
      Real x = coefficients[k];
    //WriteLn("TRACE [MPM::@PriorLinComb::BuildCombRow] 2."<<k+" "<<parameters[k]+" j="<<j+" x="<<x);
      If(j>0, [[  [[i,j,x]] ]],
      {
        Real ok := False;
        Warning("[MPM::@PriorLinComb::BuildCombRow] "+parameters[k]+" is not a "
        "parameter of module "+module::_.name);
        Copy(Empty)
      })
    })));
    If(ok,  Triplet(ijx, 1, module::_.n), Constant(0,module::_.n,0))   
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _addOrderRelation(@Module module, Text name, Text lowerParam, Text upperParam) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@PriorLinComb::_addOrderRelation] 1"); 
    Set name_ = Eval("[[Text "+ToName(name)+"=name]]");
  //WriteLn("TRACE [@PriorLinComb::_addOrderRelation] 2"); 
    Set Append(_.combNames,name_,True);
  //WriteLn("TRACE [@PriorLinComb::_addOrderRelation] 3"); 
    Set parameters = [[lowerParam,upperParam]];
    Real i = module::findParam(lowerParam);
    Real j = module::findParam(upperParam);
    If(!i, Error("[MPM::@PriorLinComb::_addOrderRelation] : "
      "Cannot find parameter "<<lowerParam+" in module "<<module::_.name));
    If(!i, Error("[MPM::@PriorLinComb::_addOrderRelation] : "
      "Cannot find parameter "<<upperParam+" in module "<<module::_.name));
    If(!And(i,j),
    {
      Error("[MPM::@PriorLinComb::_addOrderRelation] : "
      "Cannot add order relation "<<lowerParam+" << "<<upperParam);
      False
    },
    {
      Text _.latexIneq := _.latexIneq + If(TextLength(_.latexIneq),"\\\\\n","")+
      module::getLatexParamName(i)+" \leq "+module::getLatexParamName(j);
    //WriteLn("TRACE [@PriorLinComb::_addOrderRelation] 4"); 
      Set coefficients = SetOfReal(1,-1);
    //WriteLn("TRACE [@PriorLinComb::_addOrderRelation] 5"); 
      VMatrix C = @PriorLinComb::BuildCombRow(module,parameters,coefficients);
    //WriteLn("TRACE [@PriorLinComb::addOrderRelation] 6"); 
      VMatrix If(!VRows($_.comb),
      {
        $_.comb := C
      },
      {  
        $_.comb := $_.comb << C
      });
      True
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real addOrderRelation(@Module module, Text name, Text lowerParam, Text upperParam) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@PriorLinComb::addOrderRelation] 1"); 
    _addOrderRelation(module,name,lowerParam,upperParam)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real addComb(@Module module, Text name, Set parameters, Set coefficients) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Set name_ = Eval("[[Text "+ToName(name)+"=name]]");
    Set Append(_.combNames,name_,True);
    VMatrix C = @PriorLinComb::BuildCombRow(module,parameters,coefficients);
    VMatrix If(!VRows($_.comb),
    {
      $_.comb := C
    },
    {  
      $_.comb := $_.comb << C
    });
    True
  }

};

//////////////////////////////////////////////////////////////////////////////
Class @PriorLinCombBounds : @PriorLinComb
//////////////////////////////////////////////////////////////////////////////
{
  //Column matrix of averages
  @VMatrix _.lower = [[ VMatrix Constant(0,1,?) ]];
  //Column matrix of standard deviations
  @VMatrix _.upper = [[ VMatrix Constant(0,1,?) ]];

  Set _finiteLower = Copy(Empty);
  Set _finiteUpper = Copy(Empty);

  ////////////////////////////////////////////////////////////////////////////
  Real getNumIneq(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Card(_finiteLower)+Card(_finiteUpper)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static @PriorLinCombBounds New(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @PriorLinCombBounds aux = [[
     Set _finiteLower = Copy(Empty) ]];
    aux
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _addBounds(@Module module, Real lower, Real upper) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real r = VRows($_.lower)+1;
    VMatrix If(r==1,
    {
      $_.lower := Mat2VMat(Col(lower));
      $_.upper := Mat2VMat(Col(upper))
    },
    {  
      $_.lower := $_.lower << Mat2VMat(Col(lower));
      $_.upper := $_.upper << Mat2VMat(Col(upper))
    });
    Set If(IsFinite(lower), 
    {
      Append(_finiteLower,[[r]])
    });
    Set If(IsFinite(upper), 
    {
      Append(_finiteUpper,[[r]])
    });
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real addOrderRelation(
    @Module module, Text name, Text lowerParam, Text upperParam) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@PriorLinCombBounds::addOrderRelation] 1"); 
    _addOrderRelation(module,name,lowerParam,upperParam);
    _addBounds(module,-1/0,0)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real addCombBounds(
    @Module module, Text name, 
    Set parameters, Set coefficients,
    Real lower, Real upper) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@PriorLinCombBounds::addCombBounds] 1 "+name); 
    Text t = module::_.latexName;
    Text _.latexIneq := _.latexIneq + If(TextLength(_.latexIneq),"\\\\\n","")+
    If(!IsFinite(lower),"",""<<lower+" \leq ")+
    SetSum(For(1,Card(parameters),Text(Real k)
    {
      Real i = module::findParam(parameters[k]);
      If(!i,
      {
        Error("[@PriorLinCombBounds::addCombBounds] "+name+
          " cannot find parameter "+parameters[k] );
        ""
      },
      {
        Real c = coefficients[k];
    //  Text C = FormatReal(c,"+%lf");
        Text C = Case(EQ(c,1),If(k==1,"","+"),
             EQ(c,-1),"-",
             GT(c,0) & k>1,"+"<<c,
             1==1,""<<c);      
        C<<module::getLatexParamName(i) 
      })        
    })) + 
    If(!IsFinite(upper),""," \leq "<<upper);
    addComb(module,name,parameters,coefficients);
    _addBounds(module,lower,upper)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real addMatrixIneq(@Module module, Text name, VMatrix A, VMatrix lower, VMatrix upper) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@PriorLinCombBounds::addMatrixIneq] 1.1 lower:\n"<<Matrix VMat2Mat(lower,True));
  //WriteLn("TRACE [@PriorLinCombBounds::addMatrixIneq] 1.2 upper:\n"<<Matrix VMat2Mat(upper,True));
    Set For(1,VRows(A),Real(Real r)
    {
      VMatrix Ar = SubRow(A,[[r]]);
      Matrix T = VMat2Triplet(Ar);
      Set parameters = For(1,Rows(T),Text(Real k)
      {
        Real j = MatDat(T,k,2);
        module::getParamName(j)
      });
      Set coefficients = For(1,Rows(T),Real(Real k)
      {
        MatDat(T,k,3)
      });
      Real l = If(!VRows(lower),-1/0,VMatDat(lower,r,1));
      Real u = If(!VRows(upper),+1/0,VMatDat(upper,r,1));
    //WriteLn("TRACE [@PriorLinCombBounds::addMatrixIneq] 2."<<r+".1 low:\n"<<l);
    //WriteLn("TRACE [@PriorLinCombBounds::addMatrixIneq] 2."<<r+".2 upp:\n"<<u);
      If(!Card(parameters),False,
        addCombBounds(module,name+"."<<r,parameters,coefficients,l,u))
    });
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Set getIneqStatus(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set g = MatSet(VMat2Mat(inequality_constraint(z),True))[1];
    If(And(!Card(_finiteLower),!Card(_finiteUpper)),Copy(Empty),
    {
      Set names = 
      EvalSet(ExtractByIndex(_.combNames,_finiteLower),Text(Text name)
      {
        name+"_LowerBound"
      }) <<
      EvalSet(ExtractByIndex(_.combNames,_finiteUpper),Text(Text name)
      {
        name+"_UpperBound"
      });
      For(1,Card(g),Set(Real k)
      {[[
        Text CombBoundId = names[k];
        Real CombEvalLE0 = g[k];
        Real Matches = CombEvalLE0<=0
      ]]})
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix inequality_constraint(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(And(!Card(_finiteLower),!Card(_finiteUpper)),Constant(0,1,?),
    {
      VMatrix Cz = $_.comb*z;
      SubRow(($_.lower-Cz),_finiteLower) <<
      SubRow((Cz-$_.upper),_finiteUpper)
    })
  }

};

//////////////////////////////////////////////////////////////////////////////
Class @PriorLinCombNormal : @PriorLinComb
//////////////////////////////////////////////////////////////////////////////
{
  Text _.latexPrior = "";
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexPrior(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!TextLength(_.latexPrior),"",
"
\[
\begin{array}{ccc}
"+_.latexPrior+"
\end{array}
\]
")
  };
   
   //Column matrix of averages
  @VMatrix _.nu = [[ VMatrix Constant(0,1,?) ]];
  //Column matrix of standard deviations
  @VMatrix _.sigma = [[ VMatrix  Constant(0,1,?) ]];

  Set _finiteSigma = Copy(Empty);

  ////////////////////////////////////////////////////////////////////////////
  Static @PriorLinCombNormal New(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @PriorLinCombNormal aux = [[
     Set _finiteSigma = Copy(Empty) ]];
    aux
  };

  ////////////////////////////////////////////////////////////////////////////
  Real getPriorNumber(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    Card(_finiteSigma)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _addNormalPrior(Real nu, Real sigma) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real r = VRows($_.nu)+1;
    VMatrix If(r==1,
    {
      $_.nu := Mat2VMat(Col(nu));
      $_.sigma := Mat2VMat(Col(sigma))
    },
    {  
      $_.nu := $_.nu << Mat2VMat(Col(nu));
      $_.sigma := $_.sigma << Mat2VMat(Col(sigma))
    });
    Set If(IsFinite(sigma), 
    {
      Real module::_.hasDensity := True;
      Append(_finiteSigma,[[r]])
    });
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real addCombNormalPrior(
    @Module module, Text name, 
    Set parameters, Set coefficients,
    Real nu, Real sigma) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Text _.latexPrior := _.latexPrior + If(TextLength(_.latexPrior),"\\\\\n","")+
    SetSum(For(1,Card(parameters),Text(Real k)
    {
      Real i = module::findParam(parameters[k]);
      Text t = module::getLatexParamName(i);
      Real c = coefficients[k];
      Text C = Case(EQ(c,1),If(k==1,"","+"),
           EQ(c,-1),"-",
           GT(c,0) & k>1,"+"<<c,
           1==1,""<<c);      
      C<<t  

    })) + "\sim N\left("<<nu+","<<sigma+"\right) ";
    addComb(module,name,parameters,coefficients);
    _addNormalPrior(nu,sigma)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real logDens(VMatrix x) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    If(!Card(_finiteSigma),0,
    {
      VMatrix z = SubRow($_.comb*x-$_.nu, _finiteSigma);
      VMatrix s = SubRow($_.sigma,        _finiteSigma);
      @GaussianNoise::LogDensSigmaAsVector(z,s)
    })
  }

};

