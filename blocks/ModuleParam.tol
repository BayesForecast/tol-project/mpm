//////////////////////////////////////////////////////////////////////////////
// FILE    : ParamModule.tol
// PURPOSE : Implementation of Class MPM::@ModuleParam
//////////////////////////////////////////////////////////////////////////////
Class @PriorLinCombBounds;

//////////////////////////////////////////////////////////////////////////////
Class @ModuleParam : @Module
//////////////////////////////////////////////////////////////////////////////
{
  Set _paramNames = Copy(Empty);
  VMatrix _mrv = Constant(0,1,?);
  VMatrix _.lowerBound = Constant(0,1,?);
  VMatrix _.upperBound = Constant(0,1,?);
  Set _.combConstraints = Copy(Empty);
  Real _derivativeMode = False;
  Real storedInMcmc = 1;
  Real _.hasDensity = True;
  Set _.latexParamName = Copy(Empty); 

  ////////////////////////////////////////////////////////////////////////////
  Text getLatexParamName(Real k) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = Card(_.latexParamName);
    If(!LE(1,k,_.n),Error("[@ModuleParam::getLatexParamName] Cannot access to "
      <<k+"-th item of "<<n+" elements"));
    Text tex = If(And(n,LE(1,k,n)), 
      _.latexParamName[k], 
      getLatexName(?)+"_{"<<k+"}")
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexBounds(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    "\n\n\begin{tabular}{ l c r }\n"+ 
    SetSum( For(1,Card(_paramNames),Text(Real k)
    {
      Real l = VMatDat(_.lowerBound,k,1);
      Real u = VMatDat(_.upperBound,k,1);
      "$"+
      If(!IsFinite(l) & !IsFinite(u),getLatexParamName(k),
      {
        If(IsFinite(l),""<<l+"\leq ","")+
        getLatexParamName(k)+
        If(IsFinite(u),"\leq "<<u,"")
      }) +
      "$ & : & "<<Replace(_paramNames[k],"_","\_")+" \\\\\n"
    }))+
    "\end{tabular}\n\n"
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexIneq(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    If(!Card(_.combConstraints),"",$_.combConstraints::getLatexIneq(?))
  };

  VMatrix getLowerBound(Real void) { _.lowerBound };
  VMatrix getUpperBound(Real void) { _.upperBound };
  Set getCombConstraints(Real void) { _.combConstraints };

  ////////////////////////////////////////////////////////////////////////////
  Real _getNumIneq.ModuleParam(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleParam::_getNumIneq.ModuleParam] ");
    If(!Card(_.combConstraints), 0,
       $_.combConstraints::getNumIneq(?))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real getNumIneq(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleParam::getNumIneq] ");
    _getNumIneq.ModuleParam(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real SetDerivativeMode(Real dm) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    _derivativeMode := dm; True 
  };   

  ////////////////////////////////////////////////////////////////////////////
  Text getParamName(Real j) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    _paramNames[j]
  };

  ////////////////////////////////////////////////////////////////////////////
  Set getParamNames(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleParam::getParamNames] ");
    _paramNames 
  };

  ////////////////////////////////////////////////////////////////////////////
  Real findParam(Text paramName)
  ////////////////////////////////////////////////////////////////////////////
  {
    FindIndexByName(_paramNames,paramName)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real getParamValue(Text paramName)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real idx = FindIndexByName(_paramNames,paramName);
    VMatDat(_mrv,idx,1)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real setParamValue(Text paramName, Real value)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleParam::setParamValue] 1 "+paramName+":="<<value);
    Real idx = findParam(paramName);
    If(!idx,
    {
      Error("[@ModuleParam::setParamValue] Cannot find parameter "
      +paramName+" in module "+_.name);
      False
    },
    {
      VMatrix _mrv:= Convert(_mrv,"Blas.R.Dense");
      Real PutVMatDat(_mrv,idx,1,value);
      True
    })
  };
  

  ////////////////////////////////////////////////////////////////////////////
  Real addBounds(Text paramName, Real lower, Real upper)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleVAR::addBounds] 1 "+paramName);
    VMatrix If(!VRows(_.lowerBound),_.lowerBound:=Constant(_.n,1,-1/0));
    VMatrix If(!VRows(_.upperBound),_.upperBound:=Constant(_.n,1,+1/0));
    Real idx = findParam(paramName);
    If(!idx,
    {
      Error("[@ModuleParam::addBounds] Cannot find parameter "
      +paramName+" in module "+_.name);
      False
    },
    {
      VMatrix _mrv:= Convert(_mrv,"Blas.R.Dense");
      VMatrix _.lowerBound:= Convert(_.lowerBound,"Blas.R.Dense");
      VMatrix _.upperBound:= Convert(_.upperBound,"Blas.R.Dense");
      Real PutVMatDat(_.lowerBound,idx,1,lower);
      Real PutVMatDat(_.upperBound,idx,1,upper);
      Real value = VMatDat(_mrv,idx,1);
      Real If(!LE(lower,value,upper),
        PutVMatDat(_mrv,idx,1,(lower+upper)/2));
      True
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real addCombBounds(
    Text name, 
    Set parameters, Set coefficients,
    Real lower, Real upper)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock T = [[_this]];
  //WriteLn("TRACE [@ModuleVAR::addCombBounds] 1 "+name);
    If(!IsFinite(lower) & !IsFinite(upper), 
    {
    //WriteLn("TRACE [@ModuleVAR::addCombBounds] Skipping constraint "+name+" with unvalid limits ");
      False
    }, 
    {
      Set If(!Card(_.combConstraints),
      {
        _.combConstraints := @NameBlock(MPM::@PriorLinCombBounds linCombConstraints =
        [[
          Set _.combNames = Copy(Empty)
        ]])
      });
      Real $_.combConstraints::addCombBounds(
        $T, name, parameters, coefficients, lower, upper)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real addOrderRelation(Text name, Text lowerParam, Text upperParam)
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock T = [[_this]];
  //WriteLn("TRACE [@ModuleParam::addOrderRelation] 1 ("+name+","+lowerParam+","+upperParam+") "); 
    Set If(!Card(_.combConstraints),
    {
    //WriteLn("TRACE [@ModuleParam::addOrderRelation] 2"); 
      _.combConstraints := @NameBlock(MPM::@PriorLinCombBounds linCombConstraints =
      [[
        Set _.combNames = Copy(Empty)
      ]])
    });
  //WriteLn("TRACE [@ModuleParam::addOrderRelation] 3"); 
    Real $_.combConstraints::addOrderRelation(
      $T, name, lowerParam, upperParam)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real addOrderRelationSet(Set set)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set EvalSet(set,Real(BysPrior::@OrderRelation ordRel)
    {
      addOrderRelation(
        ordRel->Name, 
        ordRel->LowerParam, 
        ordRel->UpperParam)
    });
    True
  };  

  ////////////////////////////////////////////////////////////////////////////
  Real addMatrixIneq(Text name, VMatrix A, VMatrix lower, VMatrix upper)  
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock T = [[_this]];
  //WriteLn("TRACE [@ModuleParam::addMatrixIneq] 1.1 lower:\n"<<Matrix VMat2Mat(lower,True));
  //WriteLn("TRACE [@ModuleParam::addMatrixIneq] 1.2 upper:\n"<<Matrix VMat2Mat(upper,True));
    Set If(!Card(_.combConstraints),
    {
      _.combConstraints := @NameBlock(MPM::@PriorLinCombBounds linCombConstraints =
      [[
        Set _.combNames = Copy(Empty)
      ]])
    });
  //WriteLn("TRACE [@ModuleParam::addOrderRelation] 2"); 
    Real $_.combConstraints::addMatrixIneq($T, name, A, lower, upper)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real setParamNames(Set paramNames) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real _.n := Card(paramNames);
    Set _paramNames := For(1,_.n,Text(Real k)
    {
      Eval(ToName(paramNames[k])+"=\""+paramNames[k]+"\"")
    });
    SetIndexByName(_paramNames)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Check bounds
  Real check.bounds(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE ["+getMID(?)+"::check.bounds] ");
    VMatrix lw = Constant(_.n,1,-$_config::autoPrior.maxAbsoluteParamValue);
    VMatrix uw = Constant(_.n,1,+$_config::autoPrior.maxAbsoluteParamValue);
    VMatrix If(VRows(_.lowerBound)!=_.n, _.lowerBound:=lw);
    VMatrix If(VRows(_.upperBound)!=_.n, _.upperBound:=uw);
    VMatrix _.lowerBound := Max(_.lowerBound,lw);
    VMatrix _.upperBound := Min(_.upperBound,uw);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real check.size(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real n = VRows(_mrv);
    Real unk = IsUnknown(_.n);
    Real ok = Case(
    !unk & n, 
    { 
      _.n == n
    },
    !unk & !n, 
    { 
    //WriteLn("TRACE ["+getMID(?)+"::check.size] modifying _mrv to 0. _.n="<<_.n+" n="<<n+" unk="<<unk);
      If(VRows(_mrv)==_.n,True,
      {
        VMatrix _mrv := Constant(_.n, 1, 0);
        True
      })
    },
    unk & n, 
    { 
      Real _.n := n;
      True
    },
    unk & !n, 
    { 
      False
    });
    Case(!ok, False,
    Card(_paramNames)==_.n, 
    {
      setParamNames(_paramNames)
    },
    Card(_paramNames)==0,
    {
      Real setParamNames(For(1,_.n,Text(Real k)
      {
        "Var_"+FormatReal(k,"%09.0lf")
      }));
      True 
    },
    1==1, 
    {
      False
    });
    ok
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real check.param(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Real check.size(?);
    Real check.bounds(?);
    True
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real setup(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    check.param(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Initializes the Markov chain to automatic generated values
  Real setInitialValues.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    set.x(Constant(_.n,1,0))
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns current drawn values
  VMatrix get.x(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { Copy(_mrv) };

  ////////////////////////////////////////////////////////////////////////////
  //Returns current drawn values
  VMatrix _.get.x(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { _mrv };

  ////////////////////////////////////////////////////////////////////////////
  //Returns current drawn values
  @VMatrix get.x.ref(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { @VMatrix(_mrv) };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real set.x(VMatrix x) 
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleParam::set.x] "+getMID(?)+" : x:\n"<<Matrix VMat2Mat(x,True));
    VMatrix _mrv := Copy(x);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real sum.x(VMatrix x) 
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleParam::sum.x] "+getMID(?)+" x:\n"<<Matrix VMat2Mat(x,True));
    set.x(_mrv+x)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current drawn scalar value (dimension _.n must be one)
  Real setScalar(Real x) 
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleParam::setScalar] "+getMID(?)+" x:"+x);
    set.x(Mat2VMat(Col(x)))
  };

  ////////////////////////////////////////////////////////////////////////////
  Real pi_logDens_M(Matrix y) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    pi_logDens(Mat2VMat(y))
  };


  ////////////////////////////////////////////////////////////////////////////
  //Returns true if a vector match upper and lower bounds
  Real match_bounds.ModuleParam(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  { 
    And(
      If(!VRows(_.lowerBound),True,VMatSum(LT(z,_.lowerBound))==0),
      If(!VRows(_.upperBound),True,VMatSum(GT(z,_.upperBound))==0))
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix bounds_constraint.ModuleParam(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix b = 
      If(!VRows(_.lowerBound),Constant(0,1,0),_.lowerBound-z) <<
      If(!VRows(_.upperBound),Constant(0,1,0),z-_.upperBound);
    Min(Max(b,-TheMaxAbsValue),TheMaxAbsValue)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns true if a vector match upper and lower bounds
  Real match_bounds(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  { 
    match_bounds.ModuleParam(z)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix bounds_constraint(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    bounds_constraint.ModuleParam(z)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  VMatrix bounds_constraint.current(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    bounds_constraint(_mrv)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix _inequality_constraint_moduleParam(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(Or(!getNumIneq(?) , !Card(_.combConstraints)), Constant(0,1,?),
      $_.combConstraints::inequality_constraint(z))
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix inequality_constraint(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    _inequality_constraint_moduleParam(z)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  VMatrix inequality_constraint.current(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    inequality_constraint(_mrv)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real is_feasible.current(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE ["+getMID(?)+"::@ModuleParam::is_feasible.current ] 1");
    Real unknown = VMatSum(IsUnknown(_mrv))>0;
    If(unknown, False, 
    {
      Real mb = match_bounds.current(?);
    //WriteLn("TRACE ["+getMID(?)+"::is_feasible.current] 2 mb:"<<mb);
      If(!mb,False,
      {
      //WriteLn("TRACE ["+getMID(?)+"::is_feasible.current] 3 ");
        If(!getNumIneq(?),True,
        {
        //WriteLn("TRACE ["+getMID(?)+"::is_feasible.current] 4 ");
          VMatrix g = inequality_constraint.current(?);
          Real G = VMatMax(g);
          LE(G,0)
        })   
      })
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real is_feasible(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real set.x(z);
    is_feasible.current(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns log-density except a constant for a given a vector
  Real pi_logDens(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleParam::pi_logDens]");
    Real logDens = If(!is_feasible(z), -1/0, logLikelihood(z)+logPrior(z));
  //WriteLn("TRACE [MPM] 1 "+getMID(?)+"::pi_logDens :"<<logDens+"z: "<<Matrix VMat2Mat(z,True));
    If(IsUnknown(logDens),-1/0,logDens)
  };

  Real logLikelihood(VMatrix z) { 0 };
  Real logPrior     (VMatrix z) { 0 };
  Real match_bounds.current (Real void) { match_bounds(_mrv) };
  Real logLikelihood.current(Real void) { logLikelihood(_mrv) };
  Real logPrior.current     (Real void) { logPrior(_mrv) };
  Real pi_logDens.current   (Real void) { pi_logDens(_mrv) };

  ////////////////////////////////////////////////////////////////////////////
  Set getParamStatus(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set s = For(1,_.n,Set(Real k)
    {
      Real LowerBound = VMatDat(_.lowerBound,k,1);
      Real CurrentValue = VMatDat(_mrv,k,1);
      Real UpperBound = VMatDat(_.upperBound,k,1);
      Set s = [[
        Text ClassName = ClassOf(_this);
        Text Module = _.name;
        Text Parameter = _paramNames[k];
        Real IsFixed = LowerBound==UpperBound;
        Real LowerBound;
        Real CurrentValue;
        Real UpperBound;
        Real MatchLower = LowerBound<=CurrentValue;
        Real MatchUpper = UpperBound>=CurrentValue
      ]];
      Eval(Parameter+"=s")
    });
    Real SetIndexByName(s);
    s
  }

};

