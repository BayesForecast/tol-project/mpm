//////////////////////////////////////////////////////////////////////////////
// FILE    : ModuleExternNormalPrior.tol
// PURPOSE : Latent Linear module
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @ModuleExternNormalPrior : 
  @GaussianNoise,
  @ModuleNoParam,
  @ModuleExternalParam
//
//            nu = G*b + e
// 
// nu : Average of diference between internal and external filters
// b  : External parameters
// G  : Coefficients of external parameters
// e  : Normal residuals N(0,s^2)
// 
//////////////////////////////////////////////////////////////////////////////
{
  Real _.hasDensity = True;
  VMatrix _.nu = Constant(0,1,?); //External parameters 
  VMatrix _.G = Constant(0,1,?); //Coefficients of external parameters
  VMatrix _.b = Constant(0,1,?); //External parameters 
  VMatrix _.e = Constant(0,1,?); //Normal residuals N(0,s^2)
  //Number of residuals
  Real _.m=?;

  Real getDataLength(Real void) { 0 };

  ////////////////////////////////////////////////////////////////////////////
  Real getPriorNumber(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    _.m
  };


  ////////////////////////////////////////////////////////////////////////////
  Real setExternVal(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @VMatrix _.extern.val := @VMatrix(_.b);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ModuleExternNormalPrior New(
    @Estimator estimator,
    Text name,
    Set externalParameters,
    VMatrix G,
    VMatrix nu,
    Real sigma)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text expr = "MPM::@ModuleExternNormalPrior "+name+" = [[\n"+
    "  Text _.name = name;\n"+
    "  @NameBlock _config = estimator::getConfigRef(?) ]];\n";
    @NameBlock module = [[Eval(expr)]];
    VMatrix $module::_.G := G;
    VMatrix $module::_.nu := nu;
    Real $module::_.sigma := sigma;
    Real $module::setExternVal(?);
    Real $module::addExternalParameters(estimator, externalParameters);
    Real $module::setDenseExternVal(?); 
    $module
  };
 
  ////////////////////////////////////////////////////////////////////////////
  Static @NameBlock NewRef(
    @Estimator estimator,
    Text name,
    Set externalParameters,
    VMatrix G,
    VMatrix nu,
    Real sigma) 
  ////////////////////////////////////////////////////////////////////////////
  {
    @NameBlock(@ModuleExternNormalPrior::New(
      estimator, name, 
      externalParameters,
      G, nu, sigma))
  };

  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real setup(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real If(IsFinite(_.n) & IsFinite(_.m), True, {
    Real _.m := VRows(_.nu);
    VMatrix _.e := Gaussian(_.m,1,0,_.sigma); //Normal residuals N(0,s^2)
    Real check.param(?);
    True
  })};

  ////////////////////////////////////////////////////////////////////////////
  Real setNoise(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.e := z;
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns log-prior except a constant for a given a vector
  ////////////////////////////////////////////////////////////////////////////
  Real logPrior.current     (Real void) 
  { 
    VMatrix _.e := _.nu -_.G*_.b;
    @GaussianNoise::LogDensSigmaAsReal(_.e,_.sigma)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Returns log-prior except a constant for a given a vector
  ////////////////////////////////////////////////////////////////////////////
  Real logPrior(VMatrix z)
  {
    VMatrix _.e := _.nu -_.G*_.b;
    @GaussianNoise::LogDensSigmaAsReal(_.e,_.sigma)
  }
  
  

};


