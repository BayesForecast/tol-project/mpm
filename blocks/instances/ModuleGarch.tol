//////////////////////////////////////////////////////////////////////////////
// FILE    : @ModuleGarch.tol
// PURPOSE : GARCH related modules
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @HeterokedasticityFilter : @Engine
//////////////////////////////////////////////////////////////////////////////
{
  VMatrix eval.heterokedasticity(VMatrix a)
};

//////////////////////////////////////////////////////////////////////////////
Class @GarchDef : @APIComponent
//////////////////////////////////////////////////////////////////////////////
{
  Real p = 0;
  Real q = 0;
  Real hasConstant = True;
  Real hasInitRes = False;
  Real hasInitSig = False;
  Real hasAparchGamma = False;
  Real hasAparchDelta = False;
  Real aparchDelta = 2;
  Real alphaBetaMin = 0;
  Real alphaBetaMax = 0.999;
  Real logTransf = False
};

//////////////////////////////////////////////////////////////////////////////
Class @ModuleGarch : @ModuleFilter, @ModuleNormalPrior, @HeterokedasticityFilter
//////////////////////////////////////////////////////////////////////////////
{
  Real storedInMcmc = 1;
  Real _.hasDensity = False;
  //Set of factors with ARIMA struct
  
  Real _.q = ?;
  Real _.p = ?;
  Real _.alpha0 = 1;
  Polyn _.alpha = 0;
  Polyn _.beta = 1;
  Matrix _.a0 = Constant(0,0,?);
  Matrix _.s0 = Constant(0,0,?);
  Polyn _.aparchGamma = 0;
  Real _.aparchDelta = 2;
  Real hasConstant = False;
  Real hasInitRes = False;
  Real hasInitSig = False;
  Real hasAparchGamma = False;
  Real _.alphaBetaMin = 0.001;
  Real _.alphaBetaMax = 0.999;
  Real _.logTransf = False;
  
  Set _.uncVecStr = Copy(Empty);
  NameBlock _.info = [[Real _unused=?]];
  Real getDataLength(Real void) { 0 };

  Text _.latexName = "\varphi_{\sigma^2)";

  ////////////////////////////////////////////////////////////////////////////
  Static @ModuleGarch Create(Text name, @GarchDef def)
  ////////////////////////////////////////////////////////////////////////////
  {  
    @ModuleGarch aux = [[
      Text _.name = name;
      Real _.alpha0 = 1;
      Real _.q = def::q;
      Real _.p = def::p;
      Polyn _.alpha = SetSum(For(1,def::q,Polyn(Real k) { 0.01*B^k }));
      Polyn _.beta = 1-SetSum(For(1,def::p,Polyn(Real k) { 0.01*B^k }));
      Real hasConstant = def::hasConstant;
      Real hasInitRes = def::hasInitRes;
      Real hasInitSig = def::hasInitSig;
      Real hasAparchGamma = def::hasAparchGamma;
      Real hasAparchDelta = def::hasAparchDelta;
      Real _.aparchDelta = def::aparchDelta;
      Real _.alphaBetaMin = def::alphaBetaMin;
      Real _.alphaBetaMax = def::alphaBetaMax;
      Real _.logTransf = def::logTransf;
      Polyn _.aparchGamma = SetSum(For(1,def::hasAparchGamma*def::q,Polyn(Real k) { 0.01*B^k }))
    ]];
    Eval(name+"=aux")
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real getPriorNumber(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    0
  };
 
  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real setUnitaryResiduals(VMatrix a) 
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.a  := a;
    True
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set _numericInfo(Set garch)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real k = 0;
    If(hasConstant,      [[garch[k:=k+1] ]], Empty) <<
    [[ 
       Polyn garch[k:=k+1], 
       Polyn beta_ = 1-garch[k:=k+1] 
    ]] <<
    If(hasInitRes & _.q, [[garch[k:=k+1] ]], Empty) <<
    If(hasInitSig & _.p, [[garch[k:=k+1] ]], Empty) <<
    If(hasAparchGamma & _.q, [[garch[k:=k+1] ]], Empty) <<
    If(hasAparchDelta, [[garch[k:=k+1] ]], Empty)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix info2vector(Set garch)
  ////////////////////////////////////////////////////////////////////////////
  {
    Mat2VMat(GetNumeric(_numericInfo(garch)),True)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set vector2garch(VMatrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real k = 0;
    Set uncVecStr = DeepCopy(_.uncVecStr, VMat2Mat(x));
    If(hasConstant,      [[Real alpha0 = uncVecStr[k:=k+1] ]], Empty) <<
    [[
      Polyn alpha = uncVecStr[k:=k+1];
      Polyn beta = 1-uncVecStr[k:=k+1]
    ]] <<
    If(hasInitRes & _.q, [[Matrix a0   = uncVecStr[k:=k+1] ]], Empty)<<
    If(hasInitSig & _.p, [[Matrix s0   = uncVecStr[k:=k+1] ]], Empty) <<
    If(hasAparchGamma&_.q,[[Polyn aparchGamma=uncVecStr[k:=k+1] ]],Empty) <<
    If(hasAparchDelta, [[Real aparchDelta = uncVecStr[k:=k+1] ]], Empty)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real set.x(VMatrix x) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Real k = 0;
    VMatrix _mrv := Copy(x);
    Set uncVecStr = DeepCopy(_.uncVecStr, VMat2Mat(x));
    Real If(hasConstant, _.alpha0 := uncVecStr[k:=k+1] );
    Polyn _.alpha := uncVecStr[k:=k+1];
    Polyn _.beta :=-uncVecStr[k:=k+1]+1;
    Matrix If(hasInitRes & _.q, _.a0 := uncVecStr[k:=k+1] );
    Matrix If(hasInitSig & _.p, _.s0 := uncVecStr[k:=k+1] );
    Polyn If(hasAparchGamma & _.q, _.aparchGamma := uncVecStr[k:=k+1] );
    Real If(hasAparchDelta, _.aparchDelta := uncVecStr[k:=k+1] );
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Set get.garch.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Set garch = 
      If(hasConstant,       [[_.alpha0 ]], Empty) <<
      [[_.alpha, _.beta]] <<
      If(hasInitRes & _.q,  [[_.a0 ]], Empty) <<
      If(hasInitSig & _.p,  [[_.s0 ]], Empty) <<
      If(hasAparchGamma & _.q,  [[_.aparchGamma     ]], Empty) <<
      If(hasAparchDelta,  [[_.aparchDelta     ]], Empty)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real set.garch.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    set.x(info2vector(get.garch.auto(?)))
  };

  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real _setup.garch(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleGarch::setup.arma] address:"<<getMID(?));
    Real _.p := Degree(_.beta);
    Real _.q := Degree(_.alpha);
    Matrix If(Rows(_.a0)!=_.q,_.a0 := Constant(_.q,1,1));
    Matrix If(Rows(_.s0)!=_.q,_.s0 := Constant(_.p,1,1));
    Set _.uncVecStr := DeepCopy(_numericInfo(get.garch.auto(?)));
    VMatrix _mrv := Mat2VMat(GetNumeric( _.uncVecStr),True);
    Real _.n := VRows(_mrv);
    VMatrix  If(!VRows(_.lowerBound), _.lowerBound := Constant(_.n,1,   0));
    VMatrix  If(!VRows(_.upperBound), _.upperBound := Constant(_.n,1,+1/0));
    VMatrix _.lowerBound := IfVMat(IsUnknown(_.lowerBound),   0,_.lowerBound);
    VMatrix _.upperBound := IfVMat(IsUnknown(_.upperBound),+1/0,_.upperBound);
    VMatrix _.lowerBound := Max(_.lowerBound,
      Constant(hasConstant,1,0) << 
      Constant(_.q,1,_.alphaBetaMin) <<
      Constant(_.p,1,_.alphaBetaMin) <<
      Constant(hasInitRes*_.q+hasInitSig*_.p,1,+0)  <<
      Constant(hasAparchGamma * _.q,1,-1) <<
      Constant(hasAparchDelta,1,1));
    VMatrix _.upperBound := Min(_.upperBound,
      Constant(hasConstant,1,1) << 
      Constant(_.q,1,_.alphaBetaMax) <<
      Constant(_.p,1,_.alphaBetaMax) <<
      Constant(hasInitRes*_.p+hasInitSig*_.q,1,+1/0)  <<
      Constant(hasAparchGamma * _.q,1,+1) <<
      Constant(hasAparchDelta,1,+1/0)  );
    VMatrix stationarity.A =   
      Constant(1,hasConstant,0) | 
      Constant(1,_.q,1) |
      Constant(1,_.p,1) |
      Constant(1,hasInitRes*_.q+hasInitSig*_.p,0)  |
      Constant(1,hasAparchGamma * _.q,0) |
      Constant(1,hasAparchDelta,0);
    Text prefix = _.name+".";
    Set If(Card(_paramNames)!=_.n, _paramNames := 
    If(!hasConstant,Empty,[[Text prefix+"alpha.0"]]) <<
    EvalSet(Monomes(_.alpha),Text(Polyn mon)
    {
      prefix+"alpha."<<Degree(mon)
    }) <<
    EvalSet(Monomes(1-_.beta),Text(Polyn mon)
    {
      prefix+"beta."<<Degree(mon)
    }) <<
    If(!hasInitRes,Empty,For(1,_.q,Text(Real k)
    {
      prefix+"initialAbsRes."<<k
    })) <<
    If(!hasInitSig,Empty,For(1,_.p,Text(Real k)
    {
      prefix+"initialSigma."<<k
    })) <<
    If(!hasAparchGamma,Empty,
    EvalSet(Monomes(_.aparchGamma),Text(Polyn mon)
    {
      prefix+"APARCH.gamma."<<Degree(mon)
    })) <<
    If(!hasAparchDelta,Empty,[[Text prefix+"APARCH.delta"]]));
    Real setParamNames(_paramNames);
    Real addMatrixIneq (_.name+".Stationarity", 
      stationarity.A, 
      Constant(1,1,_.alphaBetaMin), 
      Constant(1,1,_.alphaBetaMax));
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real setup(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleGarch::setup] address:"<<getMID(?));
    _setup.garch(?);
  //WriteLn("TRACE [@ModuleGarch::setup] 2");
    _setup.ModuleFilter(?);
    check.param(?);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Initializes the Markov chain to automatic generated values
  Real setInitialValues.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Real _.alpha0 := 1;
    Polyn _.alpha :=  SetSum(For(1,_.q,Polyn(Real k) { 0.01*B^k }));
    Polyn _.beta := 1-SetSum(For(1,_.p,Polyn(Real k) { 0.01*B^k }));
    set.garch.auto
  };

/*
  ////////////////////////////////////////////////////////////////////////////
  //Returns true if the z is in the domain of the target density
  Real is_stationary(Polyn alpha, Polyn beta)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [ModuleGarch::is_stationary] 1");
    And(LT(EvalPol(alpha,1)+1-EvalPol(beta,1),1))
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns true if the z is in the domain of the target density
  Real is_stationary.auto(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    is_stationary(_.alpha, _.beta)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix inequality_constraint(VMatrix z) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Set info = vector2garch(z);
    _inequality_constraint_moduleParam(z) <<
    Constant(1,1,-Log(StationaryValue(info::beta))) <<
    Constant(1,1,EvalPol(info::alpha,1)-EvalPol(info::beta,1)+0.00001)
  };
    
  ////////////////////////////////////////////////////////////////////////////
  Set getIneqStatus(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix ic = inequality_constraint(_mrv);
    _getIneqStatus.Module(?)<<
    [[
      {[[
        Text CombBoundId = _.name+".beta.Stationarity";
        Real CombEvalLE0 = VMatDat(ic,1,1);
        Real Matches = CombEvalLE0<=0
      ]]},
      {[[
        Text CombBoundId = _.name+".alpha_beta.Stationarity";
        Real CombEvalLE0 = VMatDat(ic,2,1);
        Real Matches = CombEvalLE0<=0
      ]]}
    ]]
  };
*/

  ////////////////////////////////////////////////////////////////////////////
  //Evaluates heterokedasticity unitary weights
  VMatrix eval.heterokedasticity(VMatrix a) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //VMatrix filter = getFilterSum(?);
    VMatrix ad_ = If(!hasAparchGamma, 
    {
      Abs(a)^_.aparchDelta
    },
    {
      VMatrix a0 = Constant(_.q,1,0);
      VMatrix a0a = a0<<a;
      SetSum(For(1,_.q,VMatrix(Real j)
      {
        VMatrix aj = Sub(a0a,_.q-j+1,1,VRows(a),1);
        (Abs(aj)-aj*Coef(_.aparchGamma,j))^_.aparchDelta
      }))
    });
    VMatrix ad = If(_.logTransf, Log(ad_), ad_);
    Real a0 = If(_.logTransf, Log(_.alpha0^(_.aparchDelta/2)), _.alpha0^(_.aparchDelta/2));
    VMatrix ad0 = Constant(_.q,1,VMatAvr(ad));
    VMatrix ud = DifEq(_.alpha / 1, ad, ad0, Constant(0,1,?)) + a0;
    VMatrix sd0 = Constant(_.p,1,VMatAvr(ud)/EvalPol(_.beta,1));
    VMatrix hd = If(!Degree(_.beta),ud,DifEq(1 / _.beta, ud, Constant(0,1,?), sd0));
    VMatrix h_ = hd ^ (1/_.aparchDelta);
    VMatrix h = If(_.logTransf, Exp(h_), h_);
    Real nonPosRat = VMatAvr(LE(h,0))*100;
    Real If(nonPosRat,
    {
      Error("["+_.name+"::eval.heterokedasticity] "
      "There are "<<nonPosRat+"% of non positive variances with "+
      "\n  _.alpha0="<<_.alpha0<<
      "\n  _.alpha="<<_.alpha<<
      "\n  _.beta="<<_.beta<<
      "\n  _.aparchGamma="<<_.aparchGamma<<
      "\n  _.aparchDelta="<<_.aparchDelta);
      Stop
    });    
    h    
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _clean.garch (Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set _.uncVecStr := Copy(Empty);
    True 
  };

  ////////////////////////////////////////////////////////////////////////////
  Real clean (Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleGarch::_clean] ");
    _clean.garch(?);
    _clean.Module (?)
  }

};


//////////////////////////////////////////////////////////////////////////////
Class @GarchArray: @HeterokedasticityFilter
//////////////////////////////////////////////////////////////////////////////
{
  Set _.garch;
  
  ////////////////////////////////////////////////////////////////////////////
  //Evaluates heterokedasticity unitary weights
  VMatrix eval.heterokedasticity(VMatrix a) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    If(!Card(_.garch),a*0+1,
    Group("ConcatColumns", For(1,Card(_.garch),VMatrix(Real j)
    {
      (_.garch[j])::eval.heterokedasticity(SubCol(a,[[j]])) 
    })))
  }
  
};

