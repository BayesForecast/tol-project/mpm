//////////////////////////////////////////////////////////////////////////////
// FILE    : @ModuleVAR.tol
// PURPOSE : ARIMA related modules
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @ModuleVAR : @ModuleFilter, @ModuleNormalPrior
// PHI(B)*Y = F + e
//////////////////////////////////////////////////////////////////////////////
{
  Real storedInMcmc = 1;
  Real _.hasDensity = True;
  //Name of each node
  Set _.nodes = Copy(Empty);
  //Number of nodes
  Real _.dim = ?;
  //length of initial values
  Real _.length0 = ?;
  //length of original series
  Real _.length = ?;
  //Applies to each output, linear input and transfer function
  Polyn _.difference = 1;
  //Degree of differences polynomial
  Real _.difDeg = 0;
  
  //length of differenced series
  Real _.difLength = ?;
  //VAR max degree
  Real _.maxDegree = ?;
  //VAR factors
  Real _.nF = ?;
  Set _.phiFactor = Copy(Empty);
  Set _IphiFactor = Copy(Empty);
  Real _.hasConstant = False;
  Set _.cte = Copy(Empty);
  VMatrix _CTE = Constant(0,0,?);
  Real _.inequality_constraint = ?;
  Real _.isStationary = ?;

  //Matrix of VAR polynomials
  Set _.period = [[1]];
  PolMatrix _.phi = Copy(UnknownPolMatrix);
  PolMatrix _I = Copy(UnknownPolMatrix);
  
  Real _dim2Pi = ?;
  //noise initial values
  VMatrix _.Y0 = Constant(0,0,?); 
  //noise
  VMatrix _.Y = Constant(0,0,?); 
  //Covariance of simultaneous residuals as MatAlg@Cholesky reference
  Set _.cov = Copy(Empty);
  Set _.cor = Copy(Empty);
  
  //Structured set of PHI polynomials as vectors
  Set _.vecStr = Copy(Empty);
  @GarchArray _.garchArray =
  [[
    Set _.garch = Copy(Empty)
  ]];
  
  Text _.latexName = "\Phi";
  Text _.latexNoise = "Z";
  Set _.latexParamName = Copy(Empty); 
  Set _.postFilterLatexCol= Copy(Empty); 
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexEquation(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text I = If(!_.difDeg,"","I("<<_.difDeg+") ");
    Text dif = Case(
      _.difDeg==0,"",
      _.difDeg==1,"(1-B)",
      _.difDeg> 1,"(1-B)^{"<<_.difDeg+"}");
    Text cte = If(!_.hasConstant,"",
    "\left(\begin{array}{c}\n"+
    SetSum(For(1,_.dim,Text(Real k)
    {
      If(k>1,"\\\\\n","")+"C_{"<<k+"}"
    }))+
    "\n\end{array}\right)\n");
    Real filterNotNull = SetMax(EvalSet(_.postFilterLatexCol,Real(Text f) { f!="0" }));
    Text filter = Case(
      IsUnknown(filterNotNull), "",
      !filterNotNull,"",
      True,
      If(cte=="","","+")+
      "\left(\begin{array}{c}\n"+
      SetSum(For(1,_.dim,Text(Real k)
      {
        If(k>1,"\\\\\n","")+_.postFilterLatexCol[k]
      }))+
      "\n\end{array}\right)\n");
"  
\item "+I+"VAR Noise Block of order "<<_.maxDegree+" : "+_.name+"\n"
"\begin{itemize}\n"+
SetSum(For(1,_.dim,Text(Real k)
{
  "\item $"+_.latexNoise+"_{"<<k+"}"+"$ : "+_.nodes[k]+"\n"
}))+
"\end{itemize}\n"
" 
\[\n"+
If(_.maxDegree,"\\Phi\left(B\right)","")+dif+_.latexNoise+"="+cte+filter+" + e_{t}"+"
\]
\[
e_{t}\sim N\left(0, \Sigma\right) \in\mathbb{R}^{"<<_.difLength+"\\times"<<_.dim+"}
\]
"
  };
  /*
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexBounds(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    VMatrix Lp = MPM::StationaryLowerBound(_.maxDegree);
    VMatrix Up = MPM::StationaryUpperBound(_.maxDegree);
    "\n\n\begin{tabular}{ l c r }\n"+ 
    "$\left|"+getLatexName(?)+"\left(z\right)\rigth|$ \\ne 0 \forall |z|\le 1 \\\\\n"+
    SetSum( For(1,Card(_paramNames),Text(Real k)
    {
      Real l = VMatDat(_.lowerBound,k,1);
      Real u = VMatDat(_.upperBound,k,1);
      Real L = VMatDat(Lp,k,1);
      Real U = VMatDat(Up,k,1);
      If((l<=L) & (u>=U),"", v
      {
        "$"+
        If(IsFinite(l),""<<l+"\leq ","")+
        getLatexParamName(k)+
        If(IsFinite(u),"\leq "<<u,"")+
        "$ & : & "<<Replace(_paramNames[k],"_","\_")+" \\\\\n"
      }) +
      If((k%_.dim)&(k>1),
      "\end{tabular}\n"
      "\begin{tabular}{ l c r }\n",
      "")
    }))+
    "\end{tabular}\n\n"
  };
*/
  
  Real getDataLength(Real void) { _.difLength };

  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real setNoise(VMatrix z) 
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix _.Y0 := Sub(z,1,1,_.length0,_.dim);
    VMatrix _.Y  := Sub(z,1+_.length0,1,_.length,_.dim);
    True
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Changes current values of a noise series
  Real setCovariance(Text buildingMethod, VMatrix M) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Set _.cov := @NameBlock(MatAlg::@Cholesky::CreateMute(buildingMethod,M));
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Changes current values of a noise series
  Real setCorrelation(Text buildingMethod, VMatrix M) 
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [ModuleVAR::] setCorrelation");
    Set _.cor := @NameBlock(MatAlg::@Cholesky::CreateMute(buildingMethod,M));
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Changes current values of a noise series
  Real setCorrelation_decom(NameBlock svd) 
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [ModuleVAR::setCorrelation_decom] svd:\n"<<NameBlockToSet(svd));
    Set _.cor := @NameBlock(svd);
  //WriteLn("TRACE [ModuleVAR::setCorrelation_decom] _.cor:\n"<<_.cor);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix getCovariance(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(Card(_.cov),$_.cov::_.S,
    {
      VMatrix cor = $_.cor::_.S;
      cor
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix getCorrelation(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(Card(_.cor),$_.cor::_.S,
    {
      VMatrix cov = $_.cov::_.S;
      Mat2VMat(NormDiag(VMat2Mat(cov)))
    })
  };

//////////////////////////////////////////////////////////////////////////////
Static Set VarParam2MatrixSet(Matrix param)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Rows(param);
  Real pmax = Columns(param)/n;
  For(1,pmax,Matrix(Real k)
  {
    Matrix A = Sub(param,1,(k-1)*n+1,n,n);
    Eval("A"<<k+"=A")
  })
};

//////////////////////////////////////////////////////////////////////////////
Static Set VarParam2PolynMatrix(Matrix param, Real p)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Rows(param);
  Set aux = For(1,p,Set(Real k)
  {
    Matrix phi = Sub(param,1,(k-1)*n+1,n,n);
    For(1,n,Set(Real i)
    {
      For(1,n,Polyn(Real j)
      {
        MatDat(phi,i,j)*B^k
      })
    })
  });
  For(1,n,Set(Real i)
  {
    For(1,n,Polyn(Real j)
    {
      If(i==j,1,0)-
      SetSum(For(1,p,Polyn(Real k)
      {
        aux[k][i][j]
      })) 
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
Static Set PolMat2MatPol(Set PHI)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Card(PHI);
  Real pmax = SetMax(For(1,n,Real(Real i)
  {
    SetMax(For(1,n,Real(Real j)
    {
      Degree(PHI[i][j]) 
    }))
  }));
  For(0,pmax,Matrix(Real k)
  {
    Matrix phi = SetMat(For(1,n,Set(Real i)
    {
      For(1,n,Real(Real j)
      {
        Coef(PHI[i][j],k) 
      })
    }));
    Eval("phi."<<k+"=phi")
  })
};

//////////////////////////////////////////////////////////////////////////////
Static Set MatPol2PolMat(Set phi)
//////////////////////////////////////////////////////////////////////////////
{
  Real pmax = Card(phi)-1;
  Real n = Rows(phi[1]);
  Set For(1,n,Set(Real i)
  {
    For(1,n,Polyn(Real j)
    {
      SetSum(For(0,pmax,Polyn(Real k)
      {
        MatDat(phi[k+1],i,j)*B^k
      }))      
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
Static Polyn PolynMatrixDeterminant(Set polMat)
//////////////////////////////////////////////////////////////////////////////
{
  Real m = Card(polMat);
  Case(
    m==1, polMat[1][1],
    m==2, polMat[1][1]*polMat[2][2]-polMat[1][2]*polMat[2][1], 
    m==3, 
    {
      +polMat[1][1]*polMat[2][2]*polMat[3][3]
      +polMat[1][2]*polMat[2][3]*polMat[3][1]
      +polMat[1][3]*polMat[2][1]*polMat[3][2]
      -polMat[1][1]*polMat[2][3]*polMat[3][2]
      -polMat[1][2]*polMat[2][1]*polMat[3][3]
      -polMat[1][3]*polMat[2][2]*polMat[3][1]
    }, 
    1==1,
    {
      SetSum(For(1,m,Polyn(Real k)
      {
        Polyn c = polMat[k][1]*(-1)^(k+1);
        Set rows = ExtractByIndex(polMat,Range(1,m,1)-[[k]]);
        Set adjunt = EvalSet(rows,Set(Set row)
        {
          For(2,m,Polyn(Real j) { row[j] })
        });
        c*@ModuleVAR::PolynMatrixDeterminant(adjunt)
      }))
    }
  )
};
  
//////////////////////////////////////////////////////////////////////////////
Static Set PolynRootsReport(Polyn pol)
//////////////////////////////////////////////////////////////////////////////
{
  Matrix ar.roots = gsl_poly_complex_solve(pol);
  For(1,Rows(ar.roots),Set(Real k)
  {
    Set aux = [[
      Real real = MatDat(ar.roots,k,1);
      Real imaginary = MatDat(ar.roots,k,2);
      Real module = Sqrt(real^2+imaginary^2)
    ]];
    Eval("Set PHI.det.Root_"<<k+"=aux")
  })
};

/*
//////////////////////////////////////////////////////////////////////////////
 Set CalcImpulseResponseFunction(Real S)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Card(_.nodes);
  Real p = _.maxDegree;
  
  Set A = PolMat2SetMat(_.phi);
  
  Set PSI = [[ Matrix PSI0 = Diag(n,1) ]];
  
  Set For(1,S,Real(Real s)
  {
    Matrix psi = SetSum(For(1,s,Matrix(Real j)
    {
      Matrix Aj = If(j<=Card(A),A[j],Constant(n,n,0));
      Matrix psi_s_j = PSI[s-j+1];
      psi_s_j * Aj
    }));
    Set Append(PSI,IncludeText("Matrix PSI"<<s+"=psi"),True);
    True 
  });
  
  Matrix psi_ij = Group("ConcatRows",
    EvalSet(PSI,Matrix(Matrix psi) { GetNumeric([[psi]]) }));
  
  Set ij = Range(1,n,1)^2;
  
  Set IRF_1 = For(1,Card(ij),Matrix(Real k) { 
    Real i = ij[k][1];
    Real j = ij[k][2];
    Matrix psi = SubCol(psi_ij,[[k]]);
    Eval("IRF_1_"+_.nodes[j]+"_"+_.nodes[i]+"=psi")
  });
  [[ PSI, IRF_1 ]]
};
*/

  ////////////////////////////////////////////////////////////////////////////
  Set var2VectorStruct(Set phiFactor, Set cte)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleVAR::var2VectorStruct] 1 ");
    Set IphiFactor = For(1,_.nF,PolMatrix(Real k)
    {
      _I-phiFactor[k]
    });
    Set x = For(1, _.nF, Matrix(Real k)
    {
      VariableChange::CppTools::ChgVar.StationaryPolMat2Vector(
        IphiFactor[k],_IphiFactor[k])
    });
  //WriteLn("TRACE [@ModuleVAR::var2VectorStruct] 3 x:\n"<<x);
    x << If(!_.hasConstant,Empty,[[cte]])
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix var2vector(Set phiFactor, Set cte)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleVAR::var2vector] 1 ");
    Mat2VMat(GetNumeric(var2VectorStruct(phiFactor,cte)),True)
  };

  ////////////////////////////////////////////////////////////////////////////
  Static Real StationaryValue(PolMatrix phi)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix C = CompanionMatrix(phi);
    Real sv = If(!VRows(C),-1/0,
    {
      Matrix eigen = AlgLib::CppTools::rmatrixevd.eigen(VMat2Mat(C));
      Matrix eigen.module = Sqrt(eigen^2 * Col(1,1));
      Real eigen.module.max = MatMax(eigen.module);
      Real sv = Log(eigen.module.max);
/*
      Set roots := MPM::@ModuleVAR::PolynRootsReport(determinant);

      If(!IsFinite(sv) | sv>=0,
      {
      //WriteLn("TRACE [@ModuleVAR::StationaryValue] eigen.module.max="<<eigen.module.max);
      //WriteLn("TRACE [@ModuleVAR::StationaryValue] sv="<<sv);
      //WriteLn("TRACE [@ModuleVAR::StationaryValue] phi=\n"<<PolMat2TabPol(phi));
      //WriteLn("TRACE [@ModuleVAR::StationaryValue] C=\n"<<Matrix VMat2Mat(C));
      //WriteLn("TRACE [@ModuleVAR::StationaryValue] eigen=\n"<<Matrix Tra (eigen));
      //WriteLn("TRACE [@ModuleVAR::StationaryValue] eigen.module=\n"<<Matrix Tra(eigen.module))
      });
*/   
      sv     
    });
    If(IsUnknown(sv), +1/0, sv)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Static Text getName.Phi(Text i, Text j, Real k, Real d)
  ////////////////////////////////////////////////////////////////////////////
  {
    "PHI."<<i+"."<<j+".F"<<k+".D"<<d
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real _setup.var(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleVAR::_setup.var] 1");
    Real _.length0 := VRows(_.Y0);
    Real _.length := VRows(_.Y);
    Real _.dim := VColumns(_.Y);
    Real _.difDeg := Degree(_.difference);
    Real _.difLength := _.length - _.difDeg;
    Real _.filterLength := _.difLength; 
    PolMatrix _I := Diag(_.dim,1)*(B^0);    
    Real _.nF := Card(_.phiFactor);
    Real _.maxDegree := SetSum(For(1,_.nF,Real(Real k)
    {
      PolMatDegree(_.phiFactor[k])
    }));  
    Set _IphiFactor := For(1,_.nF,PolMatrix(Real k)
    {
      _I - _.phiFactor[k]
    });
    Set PHI = For(1,_.nF,Set(Real k)
    {
      PolMat2TabPol(_.phiFactor[k])
    });
    PolMatrix _.phi := BinGroup("*",_.phiFactor);
    
  //WriteLn("TRACE [@ModuleVAR::_setup.var] 7");
    Set If(_.hasConstant & !Card(_.cte), _.cte := NCopy(_.dim,Real 0));
    Real If(_.length0<_.maxDegree,
    {
      Warning("[MPM::@ModuleVAR] There are only "<<_.length0+
              " initial values and "<<_.maxDegree+" are required."
              "System will fill the matrix with zeroes.");
      VMatrix _.Y0 := Constant(_.maxDegree-_.length0,_.dim,0);
      True        
    });
    Real If(_.length0>_.maxDegree,
    {
      Warning("[MPM::@ModuleVAR] There are "<<_.length0+
              " initial values but just "<<_.maxDegree+" are required."
              "Unneeded ones will be skipped");
      VMatrix _.Y0 := Sub(1,1,_.maxDegree,_.dim);
      True        
    });
    Real _dim2Pi := _.dim*Log(2*Pi);
    VMatrix _CTE := Constant(_.length,_.dim,1);
    VMatrix _mrv := var2vector(_.phiFactor,_.cte);
  //WriteLn("TRACE [@ModuleVAR::_setup.var] 8 _mrv:\n"<<Matrix VMat2Mat(_mrv,True));
    Real _.n := VRows(_mrv);
    Real n = _.maxDegree*_.dim^2+_.hasConstant*_.dim;
  //WriteLn("TRACE [@ModuleVAR::_setup.var] 9 _.dim="<<_.dim+" _.maxDegree="<<_.maxDegree+" _.n="<<_.n+" n="<<n+" _.hasConstant="<<_.hasConstant);
    Set _.vecStr := DeepCopy(var2VectorStruct(_.phiFactor,_.cte));
    VMatrix  If(!VRows(_.lowerBound),
      _.lowerBound := Constant(_.n,1,-1/0));
    VMatrix  If(!VRows(_.upperBound),
      _.upperBound := Constant(_.n,1,+1/0));
  //WriteLn("TRACE [@ModuleVAR::_setup.var] 10 _.lowerBound:\n"<<Matrix VMat2Mat(_.lowerBound,True));
  //WriteLn("TRACE [@ModuleVAR::_setup.var] 11 _.upperBound:\n"<<Matrix VMat2Mat(_.upperBound,True));
    VMatrix lowerBound = Constant(0,1,?);
    VMatrix upperBound = Constant(0,1,?);
    VMatrix LB = MPM::StationaryLowerBound(_.maxDegree);
    VMatrix UB = MPM::StationaryUpperBound(_.maxDegree);
    Set For(1,_.nF,Real(Real k)
    {
      Set For(1,_.dim,Real(Real i)
      {
        Set For(1,_.dim,Real(Real j)
        {
          Set monomes = Monomes(PHI[k][i][j]-Coef(PHI[k][i][j],0)); 
          VMatrix lb = Mat2VMat(SetCol(EvalSet(monomes,Real(Polyn mon)
          {
            VMatDat(LB,Degree(mon),1)
          })));          
          VMatrix ub = Mat2VMat(SetCol(EvalSet(monomes,Real(Polyn mon)
          {
            VMatDat(UB,Degree(mon),1)
          })));          
          VMatrix lowerBound := lowerBound << lb;
          VMatrix upperBound := upperBound << ub;
          True
        });
       True  
      });
      True  
    });
    VMatrix If(_.hasConstant, {
        VMatrix lowerBound := lowerBound << Constant(_.dim,1,-1/0);
        VMatrix upperBound := upperBound << Constant(_.dim,1,+1/0)
    });
  //WriteLn("TRACE [@ModuleVAR::setup.var] 12 lowerBound:\n"<<Matrix VMat2Mat(lowerBound,True));
  //WriteLn("TRACE [@ModuleVAR::setup.var] 13 upperBound:\n"<<Matrix VMat2Mat(upperBound,True));
    VMatrix _.lowerBound := Max(_.lowerBound, lowerBound);
    VMatrix _.upperBound := Min(_.upperBound, upperBound);
  //WriteLn("TRACE [@ModuleVAR::setup.var] 14 _.lowerBound:\n"<<Matrix VMat2Mat(_.lowerBound,True));
  //WriteLn("TRACE [@ModuleVAR::setup.var] 15 _.upperBound:\n"<<Matrix VMat2Mat(_.upperBound,True));
  //WriteLn("TRACE [@ModuleVAR::setup.var] 16");
    Set If(Card(_paramNames)!=_.n, _paramNames := 
    SetConcat(For(1,_.nF,Set(Real k)
    {
      SetConcat(For(1,_.dim,Set(Real i)
      {
        SetConcat(For(1,_.dim,Set(Real j)
        {
          Text ni = _.nodes[i];
          Text nj = _.nodes[j];
          Polyn pol = (i==j)-PHI[k][i][j];
          EvalSet(Monomes(pol),Text(Polyn mon)
          {
            @ModuleVAR::getName.Phi(ni,nj,k,Degree(mon))
          })
        }))  
      }))
    }))
    <<If(!_.hasConstant,Empty,
    For(1,_.dim,Text(Real k)
    {
      "CTE."+_.nodes[k]
    })));
    Real setParamNames(_paramNames);    
  //WriteLn("TRACE [@ModuleVAR::_setup.var] 11");
    Set If(Card(_.latexParamName)!=_.n, _.latexParamName := 
    SetConcat(For(1,_.nF,Set(Real k)
    {
      SetConcat(For(1,_.dim,Set(Real i)
      {
        SetConcat(For(1,_.dim,Set(Real j)
        {
          Polyn pol = If(i==j,1-PHI[k][i][j],-PHI[k][i][j]);
          Text prefix = "\Phi_{"<<i+","<<j+","<<k;
          EvalSet(Monomes(pol),Text(Polyn mon)
          {
            prefix+","<<Degree(mon)+"}"
          })
        }))  
      }))
    }))
    <<If(!_.hasConstant,Empty,
    For(1,_.dim,Text(Real k)
    {
      "C_{"<<k+"}"
    })));
  //WriteLn("TRACE [@ModuleVAR::_setup.var] 12");
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real setup(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [@ModuleVAR::setup] 1 address:"<<getMID(?));
    _setup.var(?);
  //WriteLn("TRACE [@ModuleVAR::setup] 2");
    _setup.ModuleFilter(?);
  //WriteLn("TRACE [@ModuleVAR::setup] 3");
    check.param(?);
  //WriteLn("TRACE [@ModuleVAR::setup] 4");
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Initializes the Markov chain to automatic generated values
  Real setInitialValues.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {    
  //WriteLn("TRACE [@ModuleVAR::setInitialValues.auto] 1");
    VMatrix _mrv := var2vector(_.phiFactor,_.cte);
  //WriteLn("TRACE [@ModuleVAR::setInitialValues] 2 _mrv:\n"<<Matrix VMat2Mat(_mrv,True));
    True
  };
 
  ////////////////////////////////////////////////////////////////////////////
  //Sets current values
  Real set.x(VMatrix x) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Real noChange = Case(
      IsUnknown(_.isStationary), False,
      VRows(x)!=_.n, False,
      VRows(_mrv)!=_.n, False,
      HasUnknown(VMat2Mat(_mrv)), False,
      HasUnknown(VMat2Mat(x)), False,
      1==1,
      {
        Real dif = VMatMax(Abs(x-_mrv));
        If(IsUnknown(dif),False,dif==0)
      });
    If(noChange, True,
    {
    //WriteLn("TRACE [@ModuleVAR::set.x] 1 x:\n"<<Matrix VMat2Mat(x,1),"W");
      VMatrix _mrv := Copy(x);
    //WriteLn("TRACE [@ModuleVAR::set.x] 2 _mrv:\n"<<Matrix VMat2Mat(_mrv,True));

    //WriteLn("TRACE [@ModuleVAR::vector2var] 1 ");
      Matrix V = VMat2Mat(x);
    //WriteLn("TRACE [@ModuleVAR::vector2var] 2 V:\n"<<Matrix Tra(V));
      Set vecStr = DeepCopy(_.vecStr, V);
    //WriteLn("TRACE [@ModuleVAR::vector2var] 3 Card(_.vecStr):"<<Card(_.vecStr));
      Set _.phiFactor := For(1,_.nF,PolMatrix(Real k)
      {
        _I-VariableChange::CppTools::ChgVar.StationaryVector2PolMat(
           vecStr[k],_IphiFactor[k])
      });
      PolMatrix _.phi := BinGroup("*",_.phiFactor);
    //WriteLn("TRACE [@ModuleVAR::vector2var] 4 PHI:\n"<<PolMat2TabPol(phi));
      Set _.cte := If(!_.hasConstant, Copy(Empty), vecStr[_.nF+1]);
      Real _.inequality_constraint := SetMax(For(1,_.nF,Real(Real k)
      {
        @ModuleVAR::StationaryValue(_.phiFactor[k])      
      }));
      Real _.isStationary := _.inequality_constraint<0;
      True
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix getExoFilterSum(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE [getFilterSum] 1 _.dim="<<_.dim);
    Set _.exoFilter := EvalSet(_.filter,Set(Set flt)
    {
      Select(flt,Real(VMatrix f)
      {
        !TextBeginWith(Name(f),"cointegration.")
      })
    });
    If(_.dim==1,
    {
    //WriteLn("TRACE [getFilterSum] 2");
      MPM::SetSumLast(_.exoFilter[1])
    },
    {
    //WriteLn("TRACE [getFilterSum] 3");
      Group("ConcatColumns", For(1,_.dim,VMatrix(Real k) 
      { 
        VMatrix f = If(!Card(_.exoFilter[k]),
          Constant(_.filterLength,1,0),
          MPM::SetSumLast(_.exoFilter[k]));
      //WriteLn("TRACE [getFilterSum] 3."<<k+" f="<<f);
        f
      }))
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Evaluates conditioned residuals
  VMatrix eval.residuals(PolMatrix phi, Set cte) 
  ////////////////////////////////////////////////////////////////////////////
  { 
  //WriteLn("TRACE eval.residuals 1");
/*  
    VMatrix coint = getSelFilterSum(Real(VMatrix f){
      TextBeginWith(Name(f),"cointegration.") });
    VMatrix exog = getSelFilterSum(Real(VMatrix f){
      !TextBeginWith(Name(f),"cointegration.") });
  //VMatrix Z = SubstractLast(_.Y0<<_.Y, (_.Y0*0)<<exog);
*/      
    VMatrix filter = getFilterSum(?);
    VMatrix Z = _.Y0<<_.Y;
    VMatrix dZ = If(!_.difDeg,Z,
    {
      VMatrix aux = DifEq(_.difference/1,Z);
      Sub(aux,1+_.difDeg,1,VRows(Z)-_.difDeg,_.dim)
    });
    VMatrix W = Mat2VMat(phi:VMat2Mat(dZ));
    VMatrix const = If(!_.hasConstant, _CTE*0, 
        _CTE*Eye(_.dim,_.dim,0,Mat2VMat(SetRow(cte))));
    MPM::SubstractLast(MPM::SubstractLast(W,const),filter)
  };

  ////////////////////////////////////////////////////////////////////////////
  //Evaluates conditioned residuals
  VMatrix eval.residuals.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    eval.residuals(_.phi,_.cte)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real getNumIneq(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    _getNumIneq.ModuleParam(?) + 1
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix inequality_constraint.current(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    _inequality_constraint_moduleParam(_mrv) <<
    Constant(1,1,_.inequality_constraint)
  };

  ////////////////////////////////////////////////////////////////////////////
  VMatrix inequality_constraint(VMatrix z) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real set.x(z);
    inequality_constraint.current(?)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Set getIneqStatus(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix ic = inequality_constraint(_mrv);
    _getIneqStatus.Module(?) <<
    [[ 
      [[
        Text CombBoundId = _.name+".determinant.Stationarity";
        Real CombEvalLE0 = VMatDat(ic,1,1);
        Real Matches = CombEvalLE0<=0
      ]] /*,
      {[[
        Text CombBoundId = _.name+".positiveDefinite";
        Real CombEvalLE0 = VMatDat(ic,1,1);
        Real Matches = CombEvalLE0<=0
      ]]}*/
    ]]
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Evaluates heterokedasticity
  VMatrix eval.heterokedasticity(VMatrix residuals) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!Card(_.garchArray::_.garch), Constant(
      VRows(residuals),VColumns(residuals),1),
    {
      _.garchArray::eval.heterokedasticity(residuals)
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Evaluates heterokedasticity
  VMatrix eval.heterokedasticity.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!Card(_.garchArray::_.garch), Constant( _.length,_.dim,1),
    {
      eval.heterokedasticity(eval.residuals.auto(?))
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Evaluates conditioned homocedastic residuals
  VMatrix eval.homocedastic.residuals(VMatrix heterokedastic.residuals) 
  ////////////////////////////////////////////////////////////////////////////
  {
    If(!Card(_.garchArray::_.garch), heterokedastic.residuals,
    {
      u $/ h
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Evaluates conditioned homocedastic residuals
  VMatrix eval.homocedastic.residuals.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    eval.homocedastic.residuals(eval.residuals.auto(?))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Returns ARMA log-density conditioned to initial values
  Real logLikelihood.current(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@ModuleVAR::logLikelihood.current] 1");
  //WriteLn("TRACE [@ModuleVAR::logLikelihood.current] 2");
    If(!Card(_.garchArray::_.garch),
    {
    //WriteLn("TRACE [@ModuleVAR::logLikelihood.current] 3");
      Case(
      IsUnknown($_.cov::_.isGood), 
      {
      //Warning("[MPM::@ModuleVAR::logLikelihood.current] Unknown covariance status");
        -1/0
      },
      Not($_.cov::_.isGood), 
      {
      //Warning("[MPM::@ModuleVAR::logLikelihood.current] Wrong covariance status");
        -1/0
      },
      1==1,
      {
        If(Not(_.isStationary),
        {
        //Warning("[MPM::@ModuleVAR::logLikelihood.current] Non stationary");
          -1/0
        },
        {
          VMatrix e = eval.residuals(_.phi, _.cte);
          VMatrix a = Tra($_.cov::K_prod_M(Tra(e)));
          -0.5*(_.difLength*(_dim2Pi+$_.cov::log_det_S(?))+VMatSum(a^2))
        })
      })
    },
    {
    //WriteLn("TRACE [@ModuleVAR::logLikelihood.current] 4");
      Real corIsGood = Case(
      !Card(_.cor), True,
      IsUnknown($_.cor::_.isGood), 
      {
        Warning("[MPM::@ModuleVAR::logLikelihood.current] Unknown correlation status");
        False
      },
      Not($_.cor::_.isGood), 
      {
        Warning("[MPM::@ModuleVAR::logLikelihood.current] Wrong correlation status");
        False
      },
      1==1, True);
      If(!corIsGood,
      {
        Warning("[MPM::@ModuleVAR::logLikelihood.current] Non definite positive correlation");
        -1/0
      },
      {
      //WriteLn("TRACE [@ModuleVAR::logLikelihood.current] 5");
        If(Not(_.isStationary),
        {
          Warning("[MPM::@ModuleVAR::logLikelihood.current] Non stationary VAR");
          -1/0
        },
        {
        //WriteLn("TRACE [@ModuleVAR::logLikelihood.current] 6");
          VMatrix e = eval.residuals(_.phi, _.cte);
          VMatrix h = eval.heterokedasticity(e);
          Case(
          VMatMax(IsUnknown(h)), 
          {
            Warning("[MPM::@ModuleVAR::logLikelihood.current] Unknown GARCH variances are not allowed");
            -1/0
          },
          VMatMax(LE(h,0)), 
          {
            Warning("[MPM::@ModuleVAR::logLikelihood.current] Non positive GARCH variances are not allowed");
            -1/0
          },
          1==1,
          {
            VMatrix v = e $/ h;
            If(Card(_.cor),
            {
              VMatrix a = Tra($_.cor::K_prod_M(Tra(v)));
              -0.5*(_.difLength*(_dim2Pi+$_.cor::log_det_S(?))+VMatSum(a^2))
              - VMatSum(Log(h))
            },
            {
              VMatrix a = v;
              -0.5*(_.difLength*(_dim2Pi)+VMatSum(a^2))
              - VMatSum(Log(h))
            })
          })
        })
      })
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Returns ARMA log-density conditioned to initial values
  Real logLikelihood(VMatrix z)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real set.x(z);
    logLikelihood.current(?)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real clean (Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set _.cov := Copy(Empty);
    Set _.cor := Copy(Empty);
    _clean.Module (?)
  }

};

/*
//////////////////////////////////////////////////////////////////////////////
Class @ModuleVAR.ChVr : @ModuleChangeOfVariable
//////////////////////////////////////////////////////////////////////////////
{
  Real _.dim = ?;
  Real _.deg = ?;
  Set _cte_str = Copy(Empty);
  Set _smp = Copy(Empty);
  
  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real _setup.ModuleVAR.ChVr(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real _.dim := $_.auxModule::_.dim;
    Real _.deg := $_.auxModule::_.maxDegree;
    Real _.n   := $_.auxModule::_.n;
    Set _cte_str := $_.auxModule::_.cte;
    Set _smp := @NameBlock(VariableChange::@StableMatrixPolyn::Create(_.dim,_.deg));
  //WriteLn("TRACE [_setup.ModuleVAR.ChVr] 1 _.dim="<<_.dim);
  //WriteLn("TRACE [_setup.ModuleVAR.ChVr] 1 _.deg="<<_.deg);
  //WriteLn("TRACE [_setup.ModuleVAR.ChVr] 1 _.n="<<_.n);
    VMatrix  _.lowerBound := Mat2VMat(
      $_smp::stableSquareSet.LowerBounds(?))<< 
      If(Not($_.auxModule::_.hasConstant),Constant(0,1,?),
      Constant(_.dim,1,-1/0));
    VMatrix _.upperBound := Mat2VMat(
      $_smp::stableSquareSet.UpperBounds(?)) << 
      If(Not($_.auxModule::_.hasConstant),Constant(0,1,?),
      Constant(_.dim,1,+1/0));
    Set If(Card(_paramNames)!=_.n, _paramNames := 
    SetConcat(For(1,_.deg,Set(Real d)
    {
      For(1,_.dim,Text(Real k)
      {
        "PHI."<<d+".EigenValue."<<k
      }) << 
      SetConcat(For(1,_.dim,Set(Real i)
      {
        For(i+1,_.dim,Text(Real j)
        {
          "PHI."<<d+".LeftRotationAngle."<<i+"."<<j
        })
      })) << 
      SetConcat(For(1,_.dim,Set(Real i)
      {
        For(i+1,_.dim,Text(Real j)
        {
          "PHI."<<d+".RightRotationAngle."<<i+"."<<j
        })
      }))
    }))<<
    If(Not($_.auxModule::_.hasConstant),Empty,
    {
      For(1,_.dim,Text(Real k)
      {
        "CTE."+$_.auxModule::_.nodes[k]
      })
    }));
    Set If(Card(_.latexParamName)!=_.n, _.latexParamName := 
    SetConcat(For(1,_.deg,Set(Real d)
    {
      For(1,_.dim,Text(Real k)
      {
        "\lambda^{(\phi)}_{"<<d+","<<k+"}"
      }) << 
      SetConcat(For(1,_.dim,Set(Real i)
      {
        For(i+1,_.dim,Text(Real j)
        {
          "\\theta^{(\phi,R)}_{"<<d+","<<i+","<<j+"}"
        })
      })) << 
      SetConcat(For(1,_.dim,Set(Real i)
      {
        For(i+1,_.dim,Text(Real j)
        {
          "\\theta^{(\phi,Q)}_{"<<d+","<<i+","<<j+"}"
        })
      }))
    }))
    <<
    If(Not($_.auxModule::_.hasConstant),Empty,
    {
      For(1,_.dim,Text(Real k)
      {
        "C_{"<<k+"}"
      })
    }));
    Real setParamNames(_paramNames);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Check and setup the instance
  Real setup(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    Real setup.ChangeOfVariable(?);
    Real _setup.ModuleVAR.ChVr(?);
    Real check.param(?);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
    VMatrix transformVariables(VMatrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real n.phi = _.deg*_.dim^2;
    Matrix x0 = VMat2Mat(x,True);
    Matrix x_ = IfMat(x0,x0,1E-4);
  //WriteLn("TRACE [@ModuleVAR.ChVr::transformVariables] 1 _.n:"<<_.n+" _.dim:"<<_.dim+" _.deg:"<<_.deg+" n.phi:"<<n.phi);
  //WriteLn("TRACE [@ModuleVAR.ChVr::transformVariables] 2 x_:"<<Rows(x_)+"x"<<Columns(x_));
    Matrix x.phi = If($_.auxModule::_.hasConstant,
      Sub(x_,1,1,1,n.phi),
      x_);
    Matrix x.cte = If($_.auxModule::_.hasConstant,
      Sub(x_,1,n.phi+1,1,_.dim),
      Constant(0,0,?));
  //WriteLn("TRACE [@ModuleVAR.ChVr::transformVariables] 3 x.phi:"<<Rows(x.phi)+"x"<<Columns(x.phi));
  //WriteLn("TRACE [@ModuleVAR.ChVr::transformVariables] 4 x.cte:"<<Rows(x.cte)+"x"<<Columns(x.cte));
  //PolMatrix $_.auxModule::_.phi := TabPol2PolMat($_smp::duffinSchurBinFact(x.phi));    
    PolMatrix $_.auxModule::_.phi := VariableChange::CppTools::ChgVar.StationaryMatrixPolyn(x.phi,_.deg);
    //WriteLn("TRACE [@ModuleVAR.ChVr::transformVariables] 6 ");
    Set If($_.auxModule::_.hasConstant,
      $_.auxModule::_.cte := DeepCopy(_cte_str,x.cte));
  //WriteLn("TRACE [@ModuleVAR.ChVr::transformVariables] 7 cte:"<<$_.auxModule::_.cte);
    VMatrix y = $_.auxModule::var2vector($_.auxModule::_.phi,$_.auxModule::_.cte);
  //WriteLn("TRACE [@ModuleVAR.ChVr::transformVariables] 8 y:"<<Matrix VMat2Mat(y));
    y
  }

};

/* */
