//////////////////////////////////////////////////////////////////////////////
// FILE    : @ModuleGaussianVariance.tol
// PURPOSE : Inverse Scaled Chi Square
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @ModuleGaussianVariance : @ModuleParam
//////////////////////////////////////////////////////////////////////////////
{
  Real _.n = 1;
  Real storedInMcmc = 1;
  Real _.hasDensity = False;
  VMatrix _.lowerBound = Constant(1,1,MachineEpsilon);
  VMatrix _.upperBound = Constant(1,1,1/0);
  Real _.freeDeg = ?;
  Real _.prior.weight = 0;
  Real _.prior.stdErr = 0;
  Real _.prior.degrees = 0;

  Real getDataLength(Real void) { 0 };
  Real getPriorNumber(Real void) { _.prior.degrees>0 };

  Text _.latexName = "\sigma^{2}";
  
  ////////////////////////////////////////////////////////////////////////////
  Text getLatexEquation(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
"\n\item Variance Block : "+_.name+If(!_.hasDensity,"","
\[
"+_.latexName+"\sim\chi^{2}\left("<<_.prior.degrees+","<<_.prior.stdErr+"^{2}\right)
\]
")
  };
  
  ////////////////////////////////////////////////////////////////////////////
  //Initializes the Markov chain to automatic generated values
  Real setInitialValues.auto(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    set.x(Constant(_.n,1,1E-8))
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns log-density of a @ModuleGaussianVariance distribution
  Static Real LogDens(Real x, Real v, Real t2)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real vt2 = v*t2;
    +0.5*v*Log(vt2/2)
    -LogGamma(0.5*v)
    -0.5*vt2/x
    -(0.5*v+1)*Log(x) 
  };

  ////////////////////////////////////////////////////////////////////////////
  Real setInitValueAndUpperBound(Real s2, Real alpha)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real setScalar(s2);
    Real upper = s2*DistChiInv(1-alpha, _.freeDeg)/_.freeDeg;
    VMatrix _.upperBound := Constant(1,1,upper);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  //Returns log-density except a constant for a given a vector
  ////////////////////////////////////////////////////////////////////////////
  Real setPrior(Real weight, Real stdErr)
  {
    Case(
    LT(0,weight,1),
    {
      Real _.hasDensity := True;
      Real _.prior.weight := weight;
      Real _.prior.degrees := Round(_.freeDeg*weight/(1-weight));
      Real _.prior.stdErr := stdErr;
      True
    },
    EQ(weight,1),
    {
      Real _.hasDensity := False;
      Real _.prior.weight := 1;
      Real _.prior.degrees := 1/0;
      Real _.prior.stdErr := stdErr;
      VMatrix _.lowerBound := Constant(1,1,stdErr^2);
      VMatrix _.upperBound := Constant(1,1,stdErr^2);
      True
    },
    EQ(weight,0),
    {
      Real _.hasDensity := False;
      Real _.prior.weight := 0;
      Real _.prior.degrees := 0;
      Real _.prior.stdErr := ?;
      True
    },
    1==1,
    {
      Error("[MPM::@ModuleGaussianVariance::setPrior] "+_.name+
            " Cannot apply the prior weight "<<weight+ " due to it's not in interval [0,1]")
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real logPrior(VMatrix z) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    If(_.prior.weight<=0,0,
    {
      Real x = VMatDat(z,1,1);
      Real t = _.prior.stdErr^2;
      @ModuleGaussianVariance::LogDens(x, _.prior.degrees, t) 
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real getSigma(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Sqrt(VMatDat(Abs(_mrv),1,1))
  }

};


