//////////////////////////////////////////////////////////////////////////////
// FILE    : Cointegration.tol
// PURPOSE : Cointegration features and methods
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @Cointegration : @APIComponent
//////////////////////////////////////////////////////////////////////////////
{
Text _.autodoc.description = 
"Cointegration definition. "
"Default values defines no cointegration.";

Text _.autodoc.member.order = 
"If great than zero, the model is cointegrated.";
Real order = 0;

Text _.autodoc.member.hasConstant = 
"If true and cointegration::order>0, the cointegration equation has a "
"constant in each equation.";
Real hasConstant = False;

Text _.autodoc.member.beta = 
"Cointegration coefficients excluding constant.";
VMatrix beta = Constant(0,0,?);

Text _.autodoc.member.const = 
"Cointegration constants.";
VMatrix const = Constant(1,1,0);

Text _.autodoc.member.noise = 
"Cointegration noise series.";
Set noise = Copy(Empty);

//////////////////////////////////////////////////////////////////////////////
Real buildAsLinReg(Real order_, Set Y)
//////////////////////////////////////////////////////////////////////////////
{
  Real order := order;
  Real m = VRows(Y);
  Real n = VColumns(Y);
  Real col = 1;
  VMatrix beta  := Constant(n,0,?);
  VMatrix const := Constant(1,1,0);
  Set noise := Constant(m,0,?);
  While(col<=order,
  {
    Serie y = Y[col];
    Set x = ExtractByIndex(Y,Range(col+1,n,1)) << 
       If(!hasConstant,Empty, [[Serie constant=CalInd(W,Dating(y))+1]]);
    Set linReg =  LinearRegression(y, x | Constant(m,hasConstant,1));
    Matrix b = Constant(col-1,1,0) << Col(1) << 
    SetCol(For(1,Card(linReg::ParInf)-hasConstant,Real(Real j)
    {
      -linReg::ParInf[j]->Value
    }));
    Serie res = linReg[1];  
    VMatrix beta := beta | Mat2VMat(b);
    VMatrix const := Col(-linReg::ParInf[Card(linReg::ParInf)]->Value);
    Set noise := noise << [[ Eval("CointegrationNoise."<<col+"=res") ]];
    Real col := col+1
  });
  True  
};

//////////////////////////////////////////////////////////////////////////////
Static @Cointegration Default(Real void)
//////////////////////////////////////////////////////////////////////////////
{
  @Cointegration coint = [[
    Real order = 0
  ]];
  coint
};

//////////////////////////////////////////////////////////////////////////////
Static @Cointegration CreateFromLinReg(Real order_, VMatrix Y_)
//////////////////////////////////////////////////////////////////////////////
{
  @Cointegration coint = @Cointegration::Default(?);
  Real coint::buildAsLinReg(order_, Y_);
  coint
};

//////////////////////////////////////////////////////////////////////////////
Static @Cointegration CreateWithoutConstant(
  Set noise_, VMatrix beta_)
//////////////////////////////////////////////////////////////////////////////
{
  @Cointegration coint = [[
    Real order = Card(noise_);
    Real hasConstant = False;
    VMatrix beta = beta_;
    VMatrix const = Constant(1,order,0);
    Set noise = noise_
  ]];
  coint
};

//////////////////////////////////////////////////////////////////////////////
Static @Cointegration CreateWithConstant(
  Set noise_, VMatrix beta_, VMatrix const_)
//////////////////////////////////////////////////////////////////////////////
{
  @Cointegration coint = [[
    Real order = Card(noise_);
    Real hasConstant = VRows(const_) & VColumns(const_);
    VMatrix beta = beta_;
    VMatrix const = const_;
    Set noise = noise_
  ]];
  coint
};

//////////////////////////////////////////////////////////////////////////////
Static @Cointegration Union2(@Cointegration a, @Cointegration b)
//////////////////////////////////////////////////////////////////////////////
{
  @Cointegration coint = [[
    Real order = a::order + b::order;
    Real hasConstant = Or(a::hasConstant,b::hasConstant);
    VMatrix beta = a.beta | b.beta;
    VMatrix const = a.const | b.const;
    Set noise = a.noise << b.noise
  ]];
  coint
};

//////////////////////////////////////////////////////////////////////////////
Static @Cointegration Union(Set c)
//////////////////////////////////////////////////////////////////////////////
{
  @Cointegration coint = [[
    Real order = SetSum(EvalSet(c,Real(@Cointegration a) { a::order }));
    Real hasConstant = Group("Or",EvalSet(c,Real(@Cointegration a) { a::hasConstant }));
    VMatrix beta = Group("ConcatColumns",EvalSet(c,VMatrix(@Cointegration a) { a::beta }));
    VMatrix const = Group("ConcatColumns",EvalSet(c,VMatrix(@Cointegration a) { a::const }));
    Set noise = SetConcat(EvalSet(c,Set(@Cointegration a) { a::noise }))
  ]];
  coint
}



};
