//////////////////////////////////////////////////////////////////////////////
// FILE    : ARIMA.tol
// PURPOSE : ARIMA models
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @ARIMA :  @NormalReg
//////////////////////////////////////////////////////////////////////////////
{
Text _.autodoc.desfription =
"An AutoRegressive Integrated Moving Average or ARIMA model is a "
"generalization of an autoregressive moving average (ARMA) model.\n"
"The ARMA model provide a parsimonious description of a (weakly) stationary "
"stochastic process in terms of two polynomials, one for the auto-regression "
"and the second for the moving average.\n"
"Strict ARIMA models are applied in some cases where data show evidence of "
"non-stationarity, where an initial differencing step (corresponding to the "
"'integrated' part of the model) can be applied to remove the non-"
"stationarity.\n"
"These models are fitted to time series data either to better understand the "
"data or to predict future points in the series (forecasting).\n"
"The model is generally referred to as an ARIMA(p,d,q) model where parameters"
" p, d, and q are non-negative integers that refer to the order of the "
"autoregressive, integrated, and moving average parts of the model "
"respectively. ARIMA models form an important part of the Box-Jenkins "
"approach to time-series modelling.\n"
"When one of the three terms is zero, it is usual to drop 'AR', 'I' or 'MA' "
"from the acronym describing the model. For example, ARIMA(0,1,0) is I(1), "
"and ARIMA(0,0,1) is MA(1).\n";

//Set of @ARIMAStruct
Set arimaInfo = Copy(Empty);

@NameBlock _.sigma2 = [[ NameBlock [[Real _unused=? ]] ]];
@NameBlock _.arima = [[ NameBlock [[Real _unused=? ]] ]];

////////////////////////////////////////////////////////////////////////////
Text getModelClass(Real void) 
////////////////////////////////////////////////////////////////////////////
{ 
  "ARIMA"
};

////////////////////////////////////////////////////////////////////////////
Text getModelSubClass(Real void) 
////////////////////////////////////////////////////////////////////////////
{ 
  GetLabelFromArima(arimaInfo)
};

//////////////////////////////////////////////////////////////////////////////
Real setARIMAStructure(Set arima)
//////////////////////////////////////////////////////////////////////////////
{
  Set arimaInfo := DeepCopy(arima);
  True
};

//////////////////////////////////////////////////////////////////////////////
Serie transfDirect(Serie s)
//////////////////////////////////////////////////////////////////////////////
{
  If(!Card(transf),s,(transf[1])::direct_S(s))
};

//////////////////////////////////////////////////////////////////////////////
Serie transfInverse(Serie s)
//////////////////////////////////////////////////////////////////////////////
{
  If(!Card(transf),s,(transf[1])::inverse_S(s))
};

//////////////////////////////////////////////////////////////////////////////
@Module _CreateRegressionModule(@Estimator estimator)
//////////////////////////////////////////////////////////////////////////////
{
  VMatrix y = getOutputTransf_V(1);
//WriteLn("TRACE _CreateRegressionModule 2");
  @NameBlock aref = [[_this]];
//WriteLn("TRACE _CreateRegressionModule 3");
  Real m = VRows(y);
  VMatrix Y.unk := IsUnknown(y);
  Real Y.hasUnk = VMatSum(Y.unk)>0;
  VMatrix If(Y.hasUnk, y := IfVMat(Y.unk,VMatAvr(y),y));
  Polyn dif = ARIMAGetDIF(arimaInfo);
  Real d = Degree(dif);
  VMatrix Yd = Sub(DifEq(dif/1,y),d+1,1,m-d,1);
  VMatrix Yd_ = Sub(DifEq(((1-B)*dif)/1,y),d+2,1,m-d-1,1);
  Real var  = VMatVar(Yd);
//WriteLn("TRACE _CreateRegressionModule var="<<var);
  Real var_ = VMatVar(Yd_);
//WriteLn("TRACE _CreateRegressionModule var_="<<var_);
  Real Yd.var = Min(var,var_);
//WriteLn("TRACE _CreateRegressionModule 5");
  MPM::@ModuleGaussianVariance sigma2 = [[
    @NameBlock _config = estimator::getConfigRef(?);
    Text _.name = "sigma2";
    Set _paramNames = [[$aref::name+".Variance"]];
    Real _.freeDeg = m-d;
    VMatrix _mrv = Constant(1,1,Yd.var);
    VMatrix _.upperBound = Constant(1,1,Yd.var)
  ]];
  MPM::@ModuleArima arima = [[
    @NameBlock _config = estimator::getConfigRef(?);
    Text _.name = "arima";
    Set  _.factor = arimaInfo;
    VMatrix _.z = y
  ]];
  MPM::@ModuleFilteredOutput mainFilter = [[
    @NameBlock _config = estimator::getConfigRef(?);
    @VMatrix _.output = [[  getOutputAsColumn(?) ]];
    Set _.transf = transf;
    Text _.name = "mainFilter"
  ]];
  Set _.sigma2 := @NameBlock(estimator::addModule(sigma2));
  Set _.arima := @NameBlock(estimator::addModule(arima));
  Set _.mainFilter := { @NameBlock(estimator::addModule(mainFilter)) };
    Real { estimator::addRelation(MPM::@RelationFilter2Noise rel.from_filter_to_arima = [[
      @NameBlock from = [[mainFilter]];
      @NameBlock to   = [[arima]] ]]) };
    Real { estimator::addRelation(MPM::@RelationVariance2GaussianNoise rel.from_sigma2_to_arima = [[
      @NameBlock from = [[sigma2]];
      @NameBlock to   = [[arima]] ]]) };
  mainFilter
};


////////////////////////////////////////////////////////////////////////////
Set _doEvaluation(MPM::@Estimator estimator)
////////////////////////////////////////////////////////////////////////////
{
  @NameBlock aRef = [[_this]];
  Set timeInfo = DeepCopy(Y::getTimeInfo(?));
  Real difDeg = $_.arima::_.d;
  Date firstDif = Succ(timeInfo->FirstDate,timeInfo->Dating,difDeg);
  
  [[

Serie Output = {
  Y::getObj(?)[1]
};
Serie Output.Full = {
  Matrix out =  VMat2Mat($_.mainFilter::getOutput(?),True);
  MatSerSet(out,timeInfo->Dating,timeInfo->FirstDate)[1]
};

Serie Output.Transf = {
  transfDirect(Output.Full)
};

Serie Residuals = {
  Matrix res = VMat2Mat($_.arima::eval.residuals.auto(?),True);
  MatSerSet(res,timeInfo->Dating,firstDif)[1]
};

Serie DifNoise = {
  Matrix res = VMat2Mat($_.arima::_.w,True);
  MatSerSet(res,timeInfo->Dating,firstDif)[1]
};

Serie Noise = {
  Matrix res = VMat2Mat($_.arima::_.z,True);
  MatSerSet(res,timeInfo->Dating,timeInfo->FirstDate)[1]
};

Set Effects = 
EvalSet($_.mainFilter::_.filter[1],Serie(VMatrix f)
{
  Serie s = MatSerSet(VMat2Mat(f,1),timeInfo->Dating,timeInfo->FirstDate)[1];
  Eval(Name(f)+"=s")
});

Serie Filter.Transf = {
  Matrix filter = VMat2Mat($_.mainFilter::getFilterSum(?),True);
  MatSerSet(filter,timeInfo->Dating,firstDif)[1]
};

Serie Filter = {
  transfInverse(Filter.Transf)
};

Real StdErr = Sqrt(AvrS(Residuals^2));

Serie Forecast = transfInverse(transfDirect(Output.Full)-Residuals)

]]

};

//////////////////////////////////////////////////////////////////////////////
  Set calcPuntualForecasting(
    MPM::@Estimator estimator_, Date lastKnown, Real numPrev, Real alfa)
//////////////////////////////////////////////////////////////////////////////
{
  MPM::@Estimator estimator = estimator_::getUnconstrained(?);
  @NameBlock arima = [[estimator::getModule("arima")]];
  Set timeInfo = DeepCopy(Y::getTimeInfo(?));
  Real difDeg = $arima::_.d;
  Date firstDif = Succ(timeInfo->FirstDate,timeInfo->Dating,difDeg);
  Date  iniDte = firstDif;
  Serie Output.Full = {
    Matrix out =  VMat2Mat(estimator::getModule("mainFilter")::getOutput(?),True);
    MatSerSet(out,timeInfo->Dating,timeInfo->FirstDate)[1]
  };
  Serie residuals = {
    Matrix res = VMat2Mat(estimator::getModule("arima")::eval.residuals.auto(?),True);
    MatSerSet(res,timeInfo->Dating,firstDif)[1]
  };
  
  Real  sigma  = $arima::_.sigma ;
  Polyn dif    = $arima::_.dif;
  Polyn ar     = $arima::_.ar;
  Polyn ma     = $arima::_.ma;
  Polyn ari    = ar*dif;

  Serie z      = SubSer(Output.Full,iniDte,lastKnown);
  Date  first  = Succ(lastKnown,Dating(z),1);
  Date  last   = Succ(lastKnown,Dating(z),numPrev);
  Serie zero   = SubSer(CalInd(W,Dating(z)),first,last);

  Serie tZ     = getOutputTransf_S(1);

  Set   effects = SetConcat(For(1,Card(filters),Set(Real k)
  {
    @NameBlock f = [[ filters[k] ]];
    $f::evalEffectsSeries(estimator)
  }));
  Serie filter  = If(EQ(Card(effects), 0), CalInd(W,Dating(z)),SetSum(effects));
  Serie noise   = tZ - filter;

  Serie iNoise  = SubSer(noise, Max(iniDte, First(residuals)),lastKnown);
  Serie iRes    = SubSer(residuals,Max(iniDte, First(residuals)),lastKnown);

  Serie prevNoise = SolveDifEq(ma,ari,zero,residuals,noise);
  Serie tPrev     = If(EQ(Card(effects), 0),prevNoise, prevNoise + filter);
  
  Polyn  psi     = Expand(ma/ari,numPrev);
  Serie  psiSer  = SubSer(psi:Pulse(first,Dating(z)),first,last);
  Serie  psiSer2 = psiSer^2;
  Serie  error2  = DifEq((sigma^2)/(1-B),psiSer2,0);
  Serie  error   = SqRt(error2);

  Real  numSig = DistNormalInv(1-(alfa/2));
  Serie tSup   = tPrev + numSig*error;
  Serie tInf   = tPrev - numSig*error;

  Serie lastP  = SubSer(z, last, last);
  Serie transformed   = transfDirect(Y::getObj(?)[1]);
  Serie histPrev      = transfInverse(transformed-residuals);
  Serie Original      = Y::getObj(?)[1];
  Serie Prevision     = transfInverse(tPrev);
  Serie BandaSuperior = transfInverse(tSup);
  Serie BandaInferior = transfInverse(tInf);

  Serie Prevision_     = Original<<Prevision;
  Serie BandaSuperior_ = Original<<BandaSuperior;
  Serie BandaInferior_ = Original<<BandaInferior;

  [[Original,Prevision,BandaSuperior,BandaInferior,
    Prevision_, BandaSuperior_, BandaInferior_,
    effects,transformed,residuals,filter,noise,prevNoise,histPrev]]
}


};


//////////////////////////////////////////////////////////////////////////////
@Estimator CreateARIMA(Text name, VMatrix Z, Set arimaInfo)
//////////////////////////////////////////////////////////////////////////////
{
  Real m = VRows(Z);
  Polyn dif = ARIMAGetDIF(arimaInfo);
  Real d = Degree(dif);
  VMatrix z = Sub(DifEq(dif/1,Z),d+1,1,m,1);
  Real sz = VMatStDs(z);
  Real resAvrSqr = VMatAvr(z^2); 
  @NameBlock estimator = [[Eval("@Estimator "+name+"=[[Text _.name=\""+name+"\"]]")]];
  MPM::@ModuleGaussianVariance sigma2 = [[
    Real storedInMcmc = 1;
    Real _.hasDensity = False;
    Text _.name = "sigma2";
    Set _paramNames = [["Variance"]];
    Real _.n = 1;
    Real _.freeDeg = m;
    Real _.scale = resAvrSqr;
    VMatrix _mrv = Mat2VMat(Col(resAvrSqr));
    VMatrix _.lowerBound = Constant(1,1,MachineEpsilon);
    VMatrix _.upperBound = Constant(1,1,sz*2)
  ]];
  MPM::@ModuleArima arima = [[
    Real _.isGibbsModule = True;
    Real storedInMcmc = 1;
    Real _.hasDensity = True;
    Text _.name = "arima";
    VMatrix _.z = Z;
    Real _.resAvrSqr = resAvrSqr; 
    Real _.sigma = Sqrt(resAvrSqr); 
    Set _.factor = arimaInfo
  ]];
  Real $estimator::addModule(arima);
  Real $estimator::addModule(sigma2);
  Real { $estimator::addRelation(MPM::@RelationVariance2GaussianNoise rel.from_sigma2_to_arima = [[
    @NameBlock from = [[sigma2]];
    @NameBlock to   = [[arima]] ]]) };
  Real $estimator::setup(?);
  Real $estimator::set.x($estimator::build.x(?));
  $estimator
};
