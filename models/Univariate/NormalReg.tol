//////////////////////////////////////////////////////////////////////////////
// FILE    : NormalReg.tol
// PURPOSE : Normal Regression
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @NormalReg :  @GrzRegUniVar 
//////////////////////////////////////////////////////////////////////////////
{
Text _.autodoc.desfription =
"A regression model is called normal when the output, after an eventual "
"transformation, is expressed as the sum of a set of exogenous explicative "
"terms and a vector of normal residuals.";

@NameBlock _.sigma2    = [[ NameBlock [[Real _unused=? ]] ]];
@NameBlock _.residuals = [[ NameBlock [[Real _unused=? ]] ]];

  ////////////////////////////////////////////////////////////////////////////
  Text getModelClass(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    "LinReg"
  };

  ////////////////////////////////////////////////////////////////////////////
  Text getModelSubClass(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  { 
    "LinReg"
  };


  ////////////////////////////////////////////////////////////////////////////
  @Module _CreateRegressionModule.LinReg(@Estimator estimator)
  ////////////////////////////////////////////////////////////////////////////
  {
    VMatrix y = getOutputTransf_V(1);
    Real Y.var = VMatVar(y);  
    MPM::@ModuleGaussianVariance sigma2 = [[
      @NameBlock _config = estimator::getConfigRef(?);
      Text _.name = "sigma2";
      Set _paramNames = [["Variance"]];
      Real _.freeDeg = m;
      VMatrix _mrv = Constant(1,1,Y.var);
      VMatrix _.upperBound = Constant(1,1,Y.var*2)
    ]];
    MPM::@WhiteNoise residuals = [[
      @NameBlock _config = estimator::getConfigRef(?);
      Text _.name = "residuals";
      Text _.latexName = latexResidualsName;      
      VMatrix _.noise = y;
      VMatrix _.weight = weight::getAll(?)
    ]];
    MPM::@ModuleFilteredOutput mainFilter = [[
      @NameBlock _config = estimator::getConfigRef(?);
      @VMatrix _.output = [[ getOutputAsColumn(?) ]];
      Set _.transf = transf;
      Text _.name = "mainFilter";
      Text _.latexName = latexOutputName;
      Text _.latexFiltered = latexResidualsName
    ]];
    Set _.sigma2 := @NameBlock(estimator::addModule(sigma2));
    Set _.residuals := @NameBlock(estimator::addModule(residuals));
    Set _.mainFilter := { @NameBlock(estimator::addModule(mainFilter)) };
    Real { estimator::addRelation(MPM::@RelationFilter2Noise rel.from_filter_to_residuals = [[
      @NameBlock from = [[mainFilter]];
      @NameBlock to   = [[residuals]] ]]) };
    Real { estimator::addRelation(MPM::@RelationVariance2GaussianNoise rel.from_sigma2_to_residuals = [[
      @NameBlock from = [[sigma2]];
      @NameBlock to   = [[residuals]] ]]) };
    mainFilter
  };
  ////////////////////////////////////////////////////////////////////////////
  @Module _CreateRegressionModule(@Estimator estimator)
  ////////////////////////////////////////////////////////////////////////////
  { 
     _CreateRegressionModule.LinReg(estimator)  
  };
  

////////////////////////////////////////////////////////////////////////////
Set _doEvaluation(MPM::@Estimator estimator)
////////////////////////////////////////////////////////////////////////////
{
VMatrix y = getOutputAsColumn(?);
[[

Set Residuals = {[[
  Matrix res = VMat2Mat(estimator::getModule("residuals")::eval.residuals.auto(?),False)
]]};
Set StdErr =
{[[
  Real stdErr = Sqrt(MatAvr(Residuals::res^2))
]]};

Set R2 =
{[[
  Real R2 = 1- ((StdErr[1]^2) / VMatVar(y))
]]};

Set Output = 
{
[[
  Matrix out = VMat2Mat(y)
]]};
Set Forecast = 
{
[[
  Matrix fcst.transf = Output::out-Residuals::res 
]]};
Set Fit = Output << Forecast

]]}
  
  
};

