//////////////////////////////////////////////////////////////////////////////
// FILE    : Transform.ScaledBoxCox.tol
// PURPOSE : Class @Transform.ScaledBoxCox
// 
// Se amplía la transformación ScaledBoxCox de modo que su imagen sea todo el
// espacio real. Esta situación se da cuando 'first' no es ni 0 ni 1.
// En esos casos el espacio imagen es el intervalo [second, infinite)
// pudiéndose encontrar errores al usar la función inversa sobre valores
// procedentes de una distribución normal (definida en todo R).
// 
// Alternativa ScaledBoxCox (first:a, second:b)
// [a>0]
// Directa: (Sign(s+b)*Abs(s+b)**a - 1)/a
//  s+b>0 : ((s+b)**a - 1)/a
//  s+b<0 : (-(-(s+b))**a - 1)/a
// Inversa: Sign(t*a+1)*Abs(t*a+1)**(1/a) - b
//  t*a+1>0 : (t*a+1)**(1/a) - b
//  t*a+1<0 : -(-(t*a+1))**(1/a) - b
// [a==0]
// Directa: Log(s+b)
// Inversa: Exp(t)-b
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @Transform.ScaledBoxCox : @Transform
//////////////////////////////////////////////////////////////////////////////
{
  Real _.power;
  Real _.shift;
  Real _.scale;
  Static @Transform.ScaledBoxCox Create(Real power, Real shift, Real scale)
  {
    @Transform.ScaledBoxCox id = 
    [[
      Real _.power = power;
      Real _.shift = shift;
      Real _.scale = scale
    ]]
  };

  Text getFamily(Real void) { "ScaledBoxCox" };
  Text getLabel(Real void) { "ScaledBoxCox["<<_.power+","<<_.shift+","<<_.scale+"]" };

  Real direct_R (Real x)
  {
    Real x_ = x/_.scale;
    Case(
    _.power==0, Log(x_ + _.shift),
    _.power==1, x_ + _.shift - 1,
    True,       (Sign(x_+_.shift)*Abs(x_+_.shift)**_.power - 1) * (1/_.power))
  };
  Serie direct_S (Serie x)
  {
    Serie x_ = x/_.scale;
    Case(
    _.power==0, Log(x_ + _.shift),
    _.power==1, x_ + _.shift - 1,
    True,       (Sign(x_+_.shift)*Abs(x_+_.shift)**_.power - 1) * (1/_.power))
  };
  Matrix direct_M (Matrix x)
  {
    Matrix x_ = x*1/_.scale;
    Case(
    _.power==0, Log(x_ + _.shift),
    _.power==1, x_ + _.shift - 1,
    True,       (Sign(x_+_.shift)*Abs(x_+_.shift)**_.power - 1) * (1/_.power))
  };
  VMatrix direct_V (VMatrix x)
  {
    VMatrix x_ = x/_.scale;
    Case(
    _.power==0, Log(x_ + _.shift),
    _.power==1, x_ + _.shift - 1,
    True,       (Sign(x_+_.shift)*Abs(x_+_.shift)**_.power - 1) * (1/_.power))
  };
  
  Real inverse_R(Real y)
  {
    Case(
    _.power==0, Exp(y) - _.shift,
    _.power==1, y - _.shift + 1,
    True,       (Sign(y+_.second)*Abs(y+_.second)**_.first - 1) * (1/_.first))*_.scale
  };
  Serie inverse_S(Serie y)
  {
    Case(
    _.power==0, Exp(y) - _.shift,
    _.power==1, y - _.shift + 1,
    True,       (Sign(y+_.second)*Abs(y+_.second)**_.first - 1) * (1/_.first))*_.scale
  };
  Matrix inverse_M(Matrix y)
  {
    Case(
    _.power==0, Exp(y) - _.shift,
    _.power==1, y - _.shift + 1,
    True,       (Sign(y+_.second)*Abs(y+_.second)**_.first - 1) * (1/_.first))*_.scale
  };
  VMatrix inverse_V(VMatrix y)
  {
    Case(
    _.power==0, Exp(y) - _.shift,
    _.power==1, y - _.shift + 1,
    True,       (Sign(y+_.second)*Abs(y+_.second)**_.first - 1) * (1/_.first))*_.scale
  }
};
